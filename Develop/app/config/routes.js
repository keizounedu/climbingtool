const user = require('../app/controllers/user');
const project = require('../app/controllers/project');
const index = require('../app/controllers/index');
const multer = require('multer')
const path = require('path');
const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, path.join(__dirname, '../public/filestream/'))
        },
        filename: function (req, file, cb) {
            var getFileExt = function(fileName){
                var fileExt = fileName.split(".");
                if( fileExt.length === 1 || ( fileExt[0] === "" && fileExt.length === 2 ) ) {
                    return "";
                }
                return fileExt.pop();
            }
            cb(null, Date.now() + '.' + getFileExt(file.originalname))
        }
})
const upload = multer({ storage:storage})

// Router
module.exports = function(app) {
    app.route('/api/user/login/')
        .post(user.login)
    app.route('/api/user/create/')
        .put(upload.single(),user.putUserInfo)
    app.route('/api/user/info/')
        .post(user.getUserInfo)
    app.route('/api/user/edit/')
        .post(upload.single("file[]"),user.editUserInfo)
    app.route('/api/user/delete/:id')
        .delete(user.deleteUserInfo)                
    app.route('/api/project/')
        .post(project.getProjectInfo)
    app.route('/api/project/create/')
        .put(upload.single("file[]"),project.putProjectInfo)        
    app.route('/api/project/edit/')
        .post(upload.single("file[]"),project.editProjectInfo)                
    app.route('/api/project/delete/:id')
        .delete(project.deleteProjectInfo)                
    app.route('/project/:id')
        .get(project.diaplayProjectInfo)
    app.route('/*')
        .get(index.view)
}
