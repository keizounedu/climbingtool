const user = require('../models/user.js');
exports.login = function(req, res) {
  user.login(req, res)
};
exports.getUserInfo = function(req, res) {
  user.getUserInfo(req, res)
};
exports.putUserInfo = function(req, res) {
  user.putUserInfo(req, res)
};
exports.editUserInfo = function(req, res) {
  user.editUserInfo(req, res)
};
exports.deleteUserInfo = function(req, res) {
  user.deleteUserInfo(req, res)
};

