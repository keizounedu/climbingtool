const project = require('../models/project.js');
exports.getProjectInfo = function(req, res) {
  console.log(req, res)
  project.getProjectInfo(req, res)
};
exports.putProjectInfo = function(req, res) {
  project.putProjectInfo(req, res)
};
exports.editProjectInfo = function(req, res) {
  project.editProjectInfo(req, res)
};
exports.deleteProjectInfo = function(req, res) {
  project.deleteProjectInfo(req, res)
};
exports.diaplayProjectInfo = function(req, res) {
  project.diaplayProjectInfo(req, res)
}

