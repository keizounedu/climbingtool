const fs = require('fs');
const gm = require('gm').subClass({ imageMagick: true });
exports.projectImageReizer = function(target) {
    var isWider = false;
    gm(target)
        .size(function(err, size) {
            if (!err)
                if (size.width > size.height) {
                    isWider = true;
                }
        });
    if (isWider) {
        gm(target)
            .autoOrient()
            .resize(1594, 1196)
            .write(target, function(err) {
                if (!err) console.log('done');
            });
    } else {
        gm(target)
            .autoOrient()
            .resize(898, 1196)
            .write(target, function(err) {
                if (!err) console.log('done');
            });
    }
}
exports.userImageReizer = function(target) {
    var isWider = false;
    gm(target)
        .size(function(err, size) {
            if (!err)
                if (size.width > size.height) {
                    isWider = true;
                }
        });
    if (isWider) {
        gm(target)
            .autoOrient()
            .resize(200, 200)
            .write(target, function(err) {
                if (!err) console.log('done');
            });
    } else {
        gm(target)
            .autoOrient()
            .resize(200, 200)
            .write(target, function(err) {
                if (!err) console.log('done');
            });
    }
}
