const fs = require('fs');
const mongoose = require('mongoose');
const async = require('async');
const common = require('../service/common');
// const url = "mongodb://localhost:27017/openProject";
const url = "mongodb://heroku_mh131751:bu1rnjefc2f9bdoc6qr5i49p77@ds013095-a0.mlab.com:13095,ds013095-a1.mlab.com:13095/heroku_mh131751?replicaSet=rs-ds013095"
const db = mongoose.createConnection(url, function(err, res) {
    if (err) {
        console.log('Error connected: ' + url + ' - ' + err);
    } else {
        console.log('Success connected: ' + url);
    }
});
const op_projectSchema = new mongoose.Schema({
    projectName: String,
    projectGrade: String,
    projectAuthor: String,
    projectPlace: String,
    projectImg: String,
    projectImgEntity: String,
    isPublic: Boolean
}, {
    collection: 'op_project'
});
const op_project = db.model('op_project', op_projectSchema);

exports.getProjectInfo = function(req, res) {
    var q_get_project_info = req.body;
    op_project.find(q_get_project_info, {}, { sort: { _id: -1 } }, function(err, results) {
        if (results.length == 0) {
            res.status(400)
            res.end()
        } else {
            res.status(200)
            res.json(results);
        }
    });
};

exports.putProjectInfo = function(req, res) {

    var data = {
            projectName: req.body.projectName,
            projectGrade: req.body.projectGrade.replace("string:", ""),
            projectAuthor: req.body.projectAuthor,
            projectPlace: req.body.projectPlace,
            projectImg: "/filestream/" + req.file.filename,
            projectImgEntity: "",
            isPublic: req.body.isPublic == "true" ? true : false
        },
        setFileName = ""
    setNewFilePath = "";

    async.waterfall([
        (next) => {
            op_project.create(data, function(err, data) {
                if (err) throw res.status(503);
                console.log(data)
                next()
            });
        },
        (next) => {
            op_project.find({ projectImg: "/filestream/" + req.file.filename }, function(err, data) {
                var fileExtension = req.file.filename.split('.')[1];
                setFileName = data[0]._id + "." + fileExtension;
                setNewFilePath = req.file.path.replace(req.file.filename, setFileName);
                fs.renameSync(req.file.path, setNewFilePath);
                common.projectImageReizer(setNewFilePath)
                next(null, data)
            });
        },
        (resData, next) => {
            op_project.update({ "_id": resData[0]._id }, { "projectImg": "/filestream/" + setFileName, 'projectImgEntity': setNewFilePath }, { upsert: true }, function(err) {
                if (err) throw res.status(503);
                res.status(200)
                res.end()
            })
        }
    ])
};

exports.editProjectInfo = function(req, res) {
    var data = {
        projectName: req.body.projectName,
        projectGrade: req.body.projectGrade.replace("string:", ""),
        projectPlace: req.body.projectPlace,
        isPublic: req.body.isPublic == "true" ? true : false
    }
    if (req.file != undefined) {
        var fileExtension = req.file.filename.split('.')[1],
            setFileName = req.body.projectId + "." + fileExtension,
            setNewFilePath = req.file.path.replace(req.file.filename, setFileName)

        fs.renameSync(req.file.path, setNewFilePath);
        common.projectImageReizer(setNewFilePath)

        data['projectImg'] = "/filestream/" + setFileName
        data['projectImgEntity'] = setNewFilePath

    }
    op_project.update({ "_id": req.body.projectId }, data, { upsert: true }, function(err) {
        if (err) throw res.status(503);
        res.status(200)
        res.end()
    })

};

exports.deleteProjectInfo = function(req, res) {
    async.waterfall([
        (next) => {
            op_project.find({ _id: req.params.id }, function(err, data) {
                if (err) throw res.status(503);
                next(null, data)
            })
        },
        (resData, next) => {
            fs.unlink(String(resData[0].projectImgEntity), function(err) {
                if (err) throw res.status(503);
                console.log('successfully deleted ' + String(resData[0].projectImgEntity));
                next(null)
            });
        },
        (next) => {
            op_project.remove({ _id: req.params.id }, function(err) {
                if (err) throw res.status(503);
                res.status(200)
                res.end()
            })
        }
    ])
}

exports.diaplayProjectInfo = function(req, res) {
    async.waterfall([
        (next) => {
            op_project.find({ _id: req.params.id }, function(err, data) {
                if (err) throw res.status(503);
                next(null, data)
            });
        },
        (resData, next) => {
            console.log()
            if (!resData[0].isPublic) {
                res.redirect('/');
            } else {
                var pDate = {
                    og_title: resData[0].projectName + " | " + resData[0].projectGrade + " | " + resData[0].projectPlace,
                    og_description: resData[0].projectAuthor + "さんの作ったクライミングの課題です。ぜひトライしてみましょう。",
                    og_image: "https://this-is-open-project.herokuapp.com/" + resData[0].projectImg,
                    og_url: "https://this-is-open-project.herokuapp.com/project/" + req.params.id,
                    og_site_name: "Open Project",
                    projectImg: resData[0].projectImg,
                    projectPlace: resData[0].projectPlace,
                    projectAuthor: resData[0].projectAuthor,
                    projectGrade: resData[0].projectGrade,
                    projectName: resData[0].projectName
                }
                res.render('project', pDate);
            }
        }
    ])
}
