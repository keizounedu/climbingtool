const common = require('../service/common');
const fs = require('fs');
const async = require('async');
const mongoose = require('mongoose');
// const url = "mongodb://localhost:27017/openProject";
const url = "mongodb://heroku_mh131751:bu1rnjefc2f9bdoc6qr5i49p77@ds013095-a0.mlab.com:13095,ds013095-a1.mlab.com:13095/heroku_mh131751?replicaSet=rs-ds013095"
const db = mongoose.connect(url, function(err, res) {
  if (err) {
    console.log('Error connected: ' + url + ' - ' + err);
  } else {
    console.log('Success connected: ' + url);
  }
});
const op_userSchema = new mongoose.Schema({
  username: String,
  password: String,
  mailAddress: String,
  iconImg: String,
  iconImgEntity: String,
  hpUrl: String,
  introduction:String
}, {
  collection: 'op_user'
});
const op_user = db.model('op_user',op_userSchema);

const op_projectSchema = new mongoose.Schema({
  projectName: String,
  projectGrade: String,
  projectAuthor: String,
  projectPlace: String,
  projectImg: String,
  projectImgEntity: String,  
  isPublic: Boolean
}, {
  collection: 'op_project'
});
const op_project = db.model('op_project', op_projectSchema);

exports.login = function(req, res) {
  var q_login = 
  {
    username: req.body.username,
    password: req.body.password      
  }
  op_user.find(q_login, function(err, results) {
    if (results.length==0) {
      res.status(400)
      res.end()
    } else {
      res.status(200)
      res.json(results[0]);
    }
  });
};

exports.editUserInfo = function(req, res){
  var q_editUser = {
    username: req.body.username,
    password: req.body.password,
    mailAddress: req.body.mailAddress,
    hpUrl: req.body.website,
    introduction:req.body.selfIntroduce
  }
  if(req.file != undefined){
    op_user.find({username:req.body.username},function(err,data){
      var fileExtension = req.file.filename.split('.')[1],
      setFileName = data[0]._id + "." + fileExtension,
      setNewFilePath = req.file.path.replace(req.file.filename, setFileName)
      fs.renameSync(req.file.path, setNewFilePath);
      common.userImageReizer(setNewFilePath)
      q_editUser['iconImg'] = "/filestream/" + setFileName
      q_editUser['iconImgEntity'] = setNewFilePath
      op_user.update({ username: req.body.username }, q_editUser, { upsert: true }, function(err) {
        if (err) throw res.status(503);
        res.status(200)
        res.end()
      }) 
    })
  } else {
    op_user.update({ username: req.body.username }, q_editUser, { upsert: true }, function(err) {
      if (err) throw res.status(503);
      res.status(200)
      res.end()
    }) 
  }
}

exports.getUserInfo = function(req, res) {
  var q_get_user_info = {
    username: req.body.username
  }
  op_user.find(q_get_user_info, function(err, results) {
    if (results.length==0) {
      res.status(400)
      res.end()
    } else {
      res.status(200)
      res.json(results[0]);
    }
  });
};

exports.putUserInfo = function(req, res) {  
  var q_put_user = {
    username:req.body.username
  }
  async.waterfall([
    (next) => {
      op_user.find(q_put_user, function(err, results) {
        next(null,results)
      });
    },
    (resData, next) => {
      if (resData.length==0) {
        var data = {
          username:req.body.username,
          password:req.body.password,
          mailAddress:req.body.mailaddress,
          iconImg:"/assets/img/common/icon/profile.png",
          iconImgEntity:"",
          hpUrl:"",
          introduction:"",
        }
        op_user.create(data, function (err, data) {
          if (err) throw res.status(503);
          res.status(200)
          res.end()
        });
      } else {
        res.status(400)
        res.end()
      }
    }
  ])
};

exports.deleteUserInfo = function(req, res){
  async.waterfall([
    (next) => {
      op_user.find({_id:req.params.id}, function (err,userData){
        next(null,userData)
      })
    },
    (userData,next) => {
      if(userData[0].iconImgEntity != ""){
        fs.unlink(String(userData[0].iconImgEntity), function (err) {
          if (err) throw err;
          console.log('successfully deleted ' + String(userData[0].iconImgEntity));
          next(null,userData)
        })
      } else {
          next(null,userData)
      }
    },
    (userData,next) => {
      op_user.remove({_id:req.params.id}, function (err){
        if (err) throw res.status(503);
        next(null,userData)
      })
    },
    (userData,next) => {
      op_project.find({projectAuthor:userData[0].username},function (err,projectData){
        if (err) throw res.status(503);
        for(var value of projectData){
          fs.unlink(String(value.projectImgEntity), function (err) {
            if (err) throw res.status(503);
            console.log('successfully deleted ' + String(value.projectImgEntity));
          })      
        }
        next(null,userData,projectData)
      })
    },
    (userData,projectData,next) => {
      op_project.remove({projectAuthor:userData[0].username}, function (err){
        if (err) throw res.status(503);
        res.status(200)
        res.end()        
      }) 
    }
  ])
}