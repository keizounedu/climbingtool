(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

(function () {

    // init
    angular.module('App', ['ui.router', 'ngCookies', 'ngclipboard', 'ngAnimate', 'ngTouch']);

    // Service
    require("./service/http.js");
    require("./service/cookieService.js");
    require("./service/imageGenarator.js");
    require("./service/createElement.js");

    require("./service/commonService.js");
    require("./service/pushNotificationService.js");
    require("./service/messageService.js");
    require("./service/loginService.js");
    require("./service/userService.js");
    require("./service/streamService.js");
    require("./service/accountEditService.js");
    require("./service/accountRegisterService.js");
    require("./service/projectRegisterService.js");
    require("./service/projectEditService.js");

    // Components
    require("./components/login.js");
    require("./components/term.js");
    require("./components/accountRegister.js");
    require("./components/accountEdit.js");
    require("./components/user.js");
    require("./components/stream.js");
    require("./components/projectRegister.js");
    require("./components/projectEdit.js");
    require("./components/projectmaker.js");
    require("./components/pageTitle.js");
    require("./components/gnav.js");
    require("./components/pushNotification.js");

    // Router
    require("./router/router.js");
})();

},{"./components/accountEdit.js":2,"./components/accountRegister.js":3,"./components/gnav.js":4,"./components/login.js":5,"./components/pageTitle.js":6,"./components/projectEdit.js":7,"./components/projectRegister.js":8,"./components/projectmaker.js":9,"./components/pushNotification.js":10,"./components/stream.js":11,"./components/term.js":12,"./components/user.js":13,"./router/router.js":25,"./service/accountEditService.js":26,"./service/accountRegisterService.js":27,"./service/commonService.js":28,"./service/cookieService.js":29,"./service/createElement.js":30,"./service/http.js":31,"./service/imageGenarator.js":32,"./service/loginService.js":33,"./service/messageService.js":34,"./service/projectEditService.js":35,"./service/projectRegisterService.js":36,"./service/pushNotificationService.js":37,"./service/streamService.js":38,"./service/userService.js":39}],2:[function(require,module,exports){
"use strict";

var accountEdit_tpl = require("../templates/accountEdit.js").tpl,
    accountEdit_ctrl = require("../controllers/accountEdit.js").ctrl;
angular.module('App').component('accountedit', {
    controllerAs: 'accountEdit',
    template: accountEdit_tpl,
    controller: accountEdit_ctrl
});

},{"../controllers/accountEdit.js":14,"../templates/accountEdit.js":40}],3:[function(require,module,exports){
"use strict";

var accountRegister_tpl = require("../templates/accountRegister.js").tpl,
    accountRegister_ctrl = require("../controllers/accountRegister.js").ctrl;
angular.module('App').component('accountregister', {
    controllerAs: 'accountRegister',
    template: accountRegister_tpl,
    controller: accountRegister_ctrl
});

},{"../controllers/accountRegister.js":15,"../templates/accountRegister.js":41}],4:[function(require,module,exports){
"use strict";

var gnav_tpl = require("../templates/gnav.js").tpl,
    gnav_ctrl = require("../controllers/gnav.js").ctrl;
angular.module('App').component('gnav', {
    controllerAs: 'gnav',
    template: gnav_tpl,
    controller: gnav_ctrl
});

},{"../controllers/gnav.js":16,"../templates/gnav.js":42}],5:[function(require,module,exports){
"use strict";

var login_tpl = require("../templates/login.js").tpl,
    login_ctrl = require("../controllers/login.js").ctrl;
angular.module('App').component('login', {
    controllerAs: 'login',
    template: login_tpl,
    controller: login_ctrl
});

},{"../controllers/login.js":17,"../templates/login.js":43}],6:[function(require,module,exports){
'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var pageTitle_ctrl = function pageTitle_ctrl($rootScope) {
    _classCallCheck(this, pageTitle_ctrl);

    var that = this,
        meta = document.createElement('meta');
    this.title = "";
    function pageTitleChange(event, state) {
        that.title = state.title;
        document.title = state.title + ' | Open Project';
    }
    $rootScope.$on('$stateChangeSuccess', pageTitleChange);
};

angular.module('App').component('pagetitle', {
    controllerAs: 'pageTitle_ctrl',
    template: '{{pageTitle_ctrl.title}}',
    controller: pageTitle_ctrl
});

},{}],7:[function(require,module,exports){
"use strict";

var projectEdit_tpl = require("../templates/projectEdit.js").tpl,
    projectEdit_ctrl = require("../controllers/projectEdit.js").ctrl;
angular.module('App').component('projectedit', {
    controllerAs: 'projectEdit',
    template: projectEdit_tpl,
    controller: projectEdit_ctrl
});

},{"../controllers/projectEdit.js":18,"../templates/projectEdit.js":44}],8:[function(require,module,exports){
"use strict";

var projectRegister_tpl = require("../templates/projectRegister.js").tpl,
    projectRegister_ctrl = require("../controllers/projectRegister.js").ctrl;
angular.module('App').component('projectregister', {
  controllerAs: 'projectRegister',
  template: projectRegister_tpl,
  controller: projectRegister_ctrl
});

},{"../controllers/projectRegister.js":20,"../templates/projectRegister.js":46}],9:[function(require,module,exports){
"use strict";

var projectmaker_tpl = require("../templates/projectMaker.js").tpl,
    projectmaker_ctrl = require("../controllers/projectMaker.js").ctrl;
angular.module('App').component('projectmaker', {
    controllerAs: 'projectmaker',
    template: projectmaker_tpl,
    controller: projectmaker_ctrl
});

},{"../controllers/projectMaker.js":19,"../templates/projectMaker.js":45}],10:[function(require,module,exports){
"use strict";

var pushNotification_tpl = require("../templates/pushNotification.js").tpl,
    pushNotification_ctrl = require("../controllers/pushNotification.js").ctrl;
angular.module('App').component('pushnotification', {
    controllerAs: 'pushNotification',
    template: pushNotification_tpl,
    controller: pushNotification_ctrl
});

},{"../controllers/pushNotification.js":21,"../templates/pushNotification.js":47}],11:[function(require,module,exports){
"use strict";

var stream_tpl = require("../templates/stream.js").tpl,
    stream_ctrl = require("../controllers/stream.js").ctrl;
angular.module('App').component('stream', {
    controllerAs: 'stream',
    template: stream_tpl,
    controller: stream_ctrl
});

},{"../controllers/stream.js":22,"../templates/stream.js":48}],12:[function(require,module,exports){
"use strict";

var term_tpl = require("../templates/term.js").tpl,
    term_ctrl = require("../controllers/term.js").ctrl;
angular.module('App').component('term', {
    controllerAs: 'term',
    template: term_tpl,
    controller: term_ctrl
});

},{"../controllers/term.js":23,"../templates/term.js":49}],13:[function(require,module,exports){
"use strict";

var user_tpl = require("../templates/user.js").tpl,
    user_ctrl = require("../controllers/user.js").ctrl;
angular.module('App').component('user', {
    controllerAs: 'user',
    template: user_tpl,
    controller: user_ctrl
});

},{"../controllers/user.js":24,"../templates/user.js":50}],14:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

module.exports.ctrl = function () {
    /* @ngInject */

    function accountEdit_ctrl(commonService, accountEditService) {
        _classCallCheck(this, accountEdit_ctrl);

        this.cashElements(commonService, accountEditService);
        this.bindEvents();
        this.getData();
    }

    _createClass(accountEdit_ctrl, [{
        key: "cashElements",
        value: function cashElements(commonService, accountEditService) {

            this.$commonService = commonService;
            this.$accountEditService = accountEditService;

            this.file = document.getElementById("file");
            this.file_entity = undefined;

            this._id = "";
            this.username = this.$commonService.$cookieService.$cookies.get("loginId");
            this.password = "";
            this.mailAddress = "";
            this.iconImg = "";
            this.hpUrl = "";
            this.introduction = "";
            this.message = "";
            this.fd = "";
        }
    }, {
        key: "bindEvents",
        value: function bindEvents() {
            var _this = this;

            this.file.addEventListener('change', function (e) {
                var target = e.target,
                    files = target.files;
                _this.file_entity = files[0];
            });
        }
    }, {
        key: "getData",
        value: function getData() {
            var _this2 = this;

            this.$commonService.loader_visible();
            this.$commonService.$q.all([this.$accountEditService.getUserInfo(this.username)]).then(function (data) {
                _this2._id = data[0]._id;
                _this2.password = data[0].password;
                _this2.mailAddress = data[0].mailAddress;
                _this2.iconImg = data[0].iconImg;
                _this2.hpUrl = data[0].hpUrl;
                _this2.introduction = data[0].introduction;
                _this2.$commonService.loader_inVisible();
            });
        }
    }, {
        key: "edit",
        value: function edit() {
            var _this3 = this;

            this.$commonService.loader_visible();

            this.fd = new FormData(document.getElementById("accountEdit-form"));
            this.fd.append('username', String(this.username));

            if (this.file_entity == undefined) {
                this.message = '';
                this.$commonService.$q.all([this.$accountEditService.edit(this.fd)]).then(function (res) {
                    if (res) {
                        _this3.$commonService.$pushNotificationService.output(_this3.$commonService.$messageService.accoutEdit_msg_200());
                    } else {
                        _this3.$commonService.$pushNotificationService.output(_this3.$commonService.$messageService.accoutEdit_msg_400());
                    }
                    _this3.$commonService.loader_inVisible();
                });
            } else {
                if (!this.file_entity.name.match(/\.(JPG|JPEG|PNG|jpg|jpeg|png)$/) == "") {
                    this.message = '';
                    this.$commonService.$q.all([this.$accountEditService.edit(this.fd)]).then(function (res) {
                        if (res) {
                            _this3.$commonService.$pushNotificationService.output(_this3.$commonService.$messageService.accoutEdit_msg_200());
                        } else {
                            _this3.$commonService.$pushNotificationService.output(_this3.$commonService.$messageService.accoutEdit_msg_400());
                        }
                        _this3.$commonService.loader_inVisible();
                        _this3.$commonService.$state.go("userInfo", { "userId": _this3.$commonService.$cookieService.$cookies.get("loginId") });
                    });
                } else {
                    this.message = 'プロフィール写真にはjpg,pngのみ使用可能です。';
                    this.$commonService.loader_inVisible();
                }
            }
        }
    }, {
        key: "delete",
        value: function _delete() {
            var _this4 = this;

            this.$commonService.loader_visible();
            if (window.confirm('アカウントを削除しますがよろしいですか？')) {
                this.$commonService.$q.all([this.$accountEditService.delete({ _id: this._id })]).then(function (res) {
                    if (res) {
                        _this4.$commonService.$pushNotificationService.output(_this4.$commonService.$messageService.accountDelete_msg_200());
                    } else {
                        _this4.$commonService.$pushNotificationService.output(_this4.$commonService.$messageService.accountDelete_msg_400());
                    }
                    _this4.$commonService.loader_inVisible();
                    _this4.$commonService.logout();
                });
            } else {
                this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.function_interruption_msg());
                this.$commonService.loader_inVisible();
            }
        }
    }]);

    return accountEdit_ctrl;
}();

},{}],15:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

module.exports.ctrl = function () {
    /* @ngInject */

    function accountRegister_ctrl(commonService, accountRegisterService) {
        _classCallCheck(this, accountRegister_ctrl);

        this.cashElements(commonService, accountRegisterService);
    }

    _createClass(accountRegister_ctrl, [{
        key: "cashElements",
        value: function cashElements(commonService, accountRegisterService) {
            this.$commonService = commonService;
            this.$accountRegisterService = accountRegisterService;
            this.username = "";
            this.password = "";
            this.mailaddress = "";
            this.term = false;
            this.message = "";
            this.fd = "";
        }
    }, {
        key: "register",
        value: function register() {
            var _this = this;

            (function () {
                _this.$commonService.loader_visible();
                _this.fd = new FormData(document.getElementById("accountRegister-form"));
                _this.$commonService.$q.all([_this.$accountRegisterService.register(_this.fd)]).then(function (res) {
                    if (res == "complete") {
                        _this.$commonService.$pushNotificationService.output(_this.$commonService.$messageService.accoutResiger_msg_200());
                    } else {
                        _this.$commonService.$pushNotificationService.output(_this.$commonService.$messageService.accoutResiger_msg_400());
                    }
                    _this.$commonService.loader_inVisible();
                    _this.$commonService.$state.go('login');
                });
            })();
        }
    }]);

    return accountRegister_ctrl;
}();

},{}],16:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

module.exports.ctrl = function () {
    /* @ngInject */

    function gnav_ctrl(commonService) {
        _classCallCheck(this, gnav_ctrl);

        this.cashElements(commonService);
        this.$commonService.$cookieService.haveCookieCheck();
    }

    _createClass(gnav_ctrl, [{
        key: "cashElements",
        value: function cashElements(commonService) {
            this.$commonService = commonService;
            this.username = this.$commonService.$cookieService.$cookies.get("loginId");
        }
    }]);

    return gnav_ctrl;
}();

},{}],17:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

module.exports.ctrl = function () {
    /* @ngInject */

    function login_ctrl(commonService, loginService) {
        _classCallCheck(this, login_ctrl);

        this.cashElements(commonService, loginService);
    }

    _createClass(login_ctrl, [{
        key: "cashElements",
        value: function cashElements(commonService, loginService) {
            this.$commonService = commonService;
            this.$commonService.$cookieService.haveCookieCheck();
            this.$loginService = loginService;
            this.username = "";
            this.password = "";
            this.message = "";
        }
    }, {
        key: "login",
        value: function login() {
            var _this = this;

            this.$commonService.loader_visible();
            this.$commonService.$q.all([this.$loginService.login(this.username, this.password)]).then(function (msg) {
                _this.message = msg[0];
                _this.$commonService.loader_inVisible();
            });
        }
    }]);

    return login_ctrl;
}();

},{}],18:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

module.exports.ctrl = function () {
    /* @ngInject */

    function projectEdit_ctrl(commonService, projectEditService) {
        _classCallCheck(this, projectEdit_ctrl);

        if (commonService.$state.params.projectId == undefined) {
            commonService.$state.go("userInfo", { "userId": commonService.$cookies.get("loginId") });
        }
        commonService.$cookieService.$cookies.put("editProject", String(commonService.$state.params.projectId), ["expires", 7]);

        this.cashElements(commonService, projectEditService);
        this.getData();
    }

    _createClass(projectEdit_ctrl, [{
        key: "cashElements",
        value: function cashElements(commonService, projectEditService) {

            this.$commonService = commonService;
            this.$projectEditService = projectEditService;

            this.loginId = this.$commonService.$cookies.get("loginId");

            this.projectId = this.$commonService.$state.params.projectId;
            this.projectImg = "";
            this.projectName = "";
            this.projectPlace = "";
            this.projectGrade = "";
            this.projectGradeList = this.$commonService.projectGrade;
            this.projectGrade = this.projectGradeList[2];
            this.isPublic = "false";
            this.message = "";
            this.fd = "";
        }
    }, {
        key: "getData",
        value: function getData() {
            var _this = this;

            this.$commonService.loader_visible();
            this.$commonService.$q.all([this.$projectEditService.getProjectInfo({ _id: this.projectId })]).then(function (data) {
                if (data[0][0].projectAuthor != _this.loginId) {
                    _this.$commonService.$state.go("userInfo", { "userId": _this.loginId });
                }
                _this.projectImg = data[0][0].projectImg;
                _this.projectName = data[0][0].projectName;
                _this.projectPlace = data[0][0].projectPlace;
                _this.isPublic = String(data[0][0].isPublic);
                _this.projectGradeList.forEach(function (element, index, array) {
                    if (element == data[0][0].projectGrade) {
                        _this.projectGrade = _this.projectGradeList[index];
                    }
                });
                _this.$commonService.loader_inVisible();
            });
        }
    }, {
        key: "edit",
        value: function edit() {
            var _this2 = this;

            this.$commonService.loader_visible();
            this.fd = new FormData(document.getElementById("projectEdit-form"));
            this.$commonService.$q.all([this.$projectEditService.edit(this.fd)]).then(function (res) {
                if (res) {
                    _this2.$commonService.$pushNotificationService.output(_this2.$commonService.$messageService.projectEdit_msg_200());
                } else {
                    _this2.$commonService.$pushNotificationService.output(_this2.$commonService.$messageService.projectEdit_msg_400());
                }
                _this2.$commonService.loader_inVisible();
                _this2.$commonService.$state.go("userInfo", { "userId": _this2.$commonService.$cookieService.$cookies.get("loginId") });
            });
        }
    }, {
        key: "delete",
        value: function _delete() {
            var _this3 = this;

            this.$commonService.loader_visible();
            if (window.confirm('プロジェクトを削除しますがよろしいですか？')) {
                this.$commonService.$q.all([this.$projectEditService.delete({ _id: this.projectId })]).then(function (res) {
                    if (res) {
                        _this3.$commonService.$pushNotificationService.output(_this3.$commonService.$messageService.projectDelete_msg_200());
                    } else {
                        _this3.$commonService.$pushNotificationService.output(_this3.$commonService.$messageService.projectDelete_msg_400());
                    }
                    _this3.$commonService.loader_inVisible();
                    _this3.$commonService.$state.go("userInfo", { "userId": _this3.$commonService.$cookieService.$cookies.get("loginId") });
                });
            } else {
                this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.function_interruption_msg());
                this.$commonService.loader_inVisible();
            }
        }
    }]);

    return projectEdit_ctrl;
}();

},{}],19:[function(require,module,exports){
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

module.exports.ctrl =
/* @ngInject */
function projectmaker_ctrl(imageGenarator, createElements, commonService) {
    _classCallCheck(this, projectmaker_ctrl);

    imageGenarator.cashElements();
    imageGenarator.bindEvents();
    createElements.cacheElements();
    createElements.bindEvents();
};

},{}],20:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

module.exports.ctrl = function () {
    /* @ngInject */

    function projectRegister_ctrl(commonService, projectRegisterService) {
        _classCallCheck(this, projectRegister_ctrl);

        this.cashElements(commonService, projectRegisterService);
        this.bindEvents();
    }

    _createClass(projectRegister_ctrl, [{
        key: "cashElements",
        value: function cashElements(commonService, projectRegisterService) {

            this.$commonService = commonService;
            this.$projectRegisterService = projectRegisterService;

            this.file = document.getElementById("file");
            this.file_entity = undefined;

            this.projectName = "";
            this.projectPlace = "";
            this.projectGrade = "";
            this.projectGradeList = this.$commonService.projectGrade;
            this.isPublic = "false";
            this.message = "";
            this.fd = "";
        }
    }, {
        key: "bindEvents",
        value: function bindEvents() {
            var _this = this;

            this.file.addEventListener('change', function (e) {
                var target = e.target,
                    files = target.files;
                _this.file_entity = files[0];
            });
        }
    }, {
        key: "register",
        value: function register() {
            var _this2 = this;

            this.$commonService.loader_visible();
            this.fd = new FormData(document.getElementById("projectRegister-form"));
            this.fd.append('projectAuthor', this.$commonService.$cookieService.$cookies.get("loginId"));
            if (this.file_entity == undefined) {
                this.$commonService.$q.all([this.$projectRegisterService.register(this.fd)]).then(function (res) {
                    if (res) {
                        _this2.$commonService.$pushNotificationService.output(_this2.$commonService.$messageService.projectRegister_msg_200());
                    } else {
                        _this2.$commonService.$pushNotificationService.output(_this2.$commonService.$messageService.projectRegister_msg_400());
                    }
                    _this2.$commonService.loader_inVisible();
                    _this2.$commonService.$state.go("userInfo", { "userId": _this2.$commonService.$cookieService.$cookies.get("loginId") });
                });
            } else {
                if (!this.file_entity.name.match(/\.(JPG|JPEG|PNG|jpg|jpeg|png)$/) == "") {
                    this.$commonService.$q.all([this.$projectRegisterService.register(this.fd)]).then(function (res) {
                        if (res) {
                            _this2.$commonService.$pushNotificationService.output(_this2.$commonService.$messageService.projectRegister_msg_200());
                        } else {
                            _this2.$commonService.$pushNotificationService.output(_this2.$commonService.$messageService.projectRegister_msg_400());
                        }
                        _this2.$commonService.loader_inVisible();

                        _this2.$commonService.$state.go("userInfo", { "userId": _this2.$commonService.$cookieService.$cookies.get("loginId") });
                    });
                } else {
                    this.message = 'プロジェクトの写真にはjpg,pngのみ使用可能です。';
                    this.$commonService.loader_inVisible();
                }
            }
        }
    }]);

    return projectRegister_ctrl;
}();

},{}],21:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

module.exports.ctrl = function () {
    /* @ngInject */

    function gnav_ctrl(commonService) {
        _classCallCheck(this, gnav_ctrl);

        this.cashElements(commonService);
    }

    _createClass(gnav_ctrl, [{
        key: "cashElements",
        value: function cashElements(commonService) {
            this.$commonService = commonService;
        }
    }]);

    return gnav_ctrl;
}();

},{}],22:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

module.exports.ctrl = function () {
    /* @ngInject */

    function stream_ctrl(commonService, streamService) {
        _classCallCheck(this, stream_ctrl);

        this.cashElements(commonService, streamService);
        this.getData();
    }

    _createClass(stream_ctrl, [{
        key: "cashElements",
        value: function cashElements(commonService, streamService) {
            this.$commonService = commonService;
            this.$streamService = streamService;
            this.username = this.$commonService.$cookieService.$cookies.get("loginId");
        }
    }, {
        key: "getData",
        value: function getData() {
            var _this = this;

            this.$commonService.loader_visible();
            this.$commonService.$q.all([this.$streamService.getProjectInfo()]).then(function (data) {
                _this.date = new Date().getTime();
                _this.projectList = data[0];
                _this.$commonService.loader_inVisible();
            });
        }
    }]);

    return stream_ctrl;
}();

},{}],23:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

module.exports.ctrl = function () {
    /* @ngInject */

    function login_ctrl(commonService) {
        _classCallCheck(this, login_ctrl);

        this.cashElements(commonService);
    }

    _createClass(login_ctrl, [{
        key: "cashElements",
        value: function cashElements(commonService) {
            this.$commonService = commonService;
            this.$commonService.$cookieService.haveCookieCheck();
        }
    }]);

    return login_ctrl;
}();

},{}],24:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

module.exports.ctrl = function () {
    /* @ngInject */

    function user_ctrl(commonService, userService) {
        _classCallCheck(this, user_ctrl);

        this.cashElements(commonService, userService);
        this.getData();
    }

    _createClass(user_ctrl, [{
        key: "cashElements",
        value: function cashElements(commonService, userService) {
            this.$commonService = commonService;
            this.$userService = userService;

            this.loginId = this.$commonService.$cookieService.$cookies.get("loginId");
            this.username = this.$commonService.$state.params.userId;
            this.iconImg = "";
            this.hpUrl = "";
            this.introduction = "";
            this.projectList = "";

            this.date = new Date().getTime();
        }
    }, {
        key: "getData",
        value: function getData() {
            var _this = this;

            this.$commonService.loader_visible();
            this.$commonService.$q.all([this.$userService.getUserInfo(this.username), this.$userService.getProjectInfo({ projectAuthor: this.username })]).then(function (data) {
                _this.username = data[0].username;
                _this.iconImg = data[0].iconImg;
                _this.hpUrl = data[0].hpUrl;
                _this.introduction = data[0].introduction;
                if (!data[1].length == 0) {
                    _this.date = new Date().getTime();
                    _this.projectList = data[1];
                }
                _this.$commonService.loader_inVisible();
            });
        }
    }]);

    return user_ctrl;
}();

},{}],25:[function(require,module,exports){
'use strict';

angular.module('App').config(function ($locationProvider, $stateProvider, $urlRouterProvider) {
    $locationProvider.html5Mode(true).hashPrefix('*');
    $urlRouterProvider.otherwise('/login');
    $stateProvider.state('login', {
        url: "/login",
        views: {
            "mainContens": { template: "<login></login>" },
            "glovalNavigation": { template: '' }
        },
        title: 'Login'
    }).state('term', {
        url: "/term",
        views: {
            "mainContens": { template: "<term></term>" },
            "glovalNavigation": { template: '' }
        },
        title: 'Term'
    }).state('accountRegister', {
        url: "/accountRegister",
        views: {
            "mainContens": { template: "<accountregister>" },
            "glovalNavigation": { template: '' }
        },
        title: 'Account Register'
    }).state('accountEdit', {
        url: "/accountEdit",
        views: {
            "mainContens": { template: "<accountedit>" },
            "glovalNavigation": { template: "<gnav>" }
        },
        title: 'Account Edit'
    }).state('projectRegister', {
        url: "/projectRegister",
        views: {
            "mainContens": { template: "<projectregister>" },
            "glovalNavigation": { template: "<gnav>" }
        },
        title: 'Project Register'
    }).state('userInfo', {
        url: "/user/:userId",
        views: {
            "mainContens": { template: "<user>" },
            "glovalNavigation": { template: "<gnav>" }
        },
        title: 'User Information'
    }).state('projectEdit', {
        url: "/projectEdit/:projectId",
        views: {
            "mainContens": { template: "<projectedit>" },
            "glovalNavigation": { template: "<gnav>" }
        },
        title: 'Project Edit'
    }).state('stream', {
        url: "/stream",
        views: {
            "mainContens": { template: "<stream>" },
            "glovalNavigation": { template: "<gnav>" }
        },
        title: 'Stream'
    }).state('projectMaker', {
        url: "/projectMaker",
        views: {
            "mainContens": { template: "<projectmaker>" },
            "glovalNavigation": { template: "<gnav>" }
        },
        template: "<create>",
        title: 'Project Maker'
    });
});

},{}],26:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    'use strict';

    var accountEditService = function () {
        function accountEditService(commonService, messageService) {
            _classCallCheck(this, accountEditService);

            this.$commonService = commonService;
            this.$messageService = messageService;
        }

        _createClass(accountEditService, [{
            key: 'getUserInfo',
            value: function getUserInfo(username) {
                var d = this.$commonService.$q.defer(),
                    data = this.$commonService.$http.getUserInfo(username);
                data.success(function (response) {
                    d.resolve(response);
                }).error(function (response) {
                    d.resolve(response);
                });
                return d.promise;
            }
        }, {
            key: 'edit',
            value: function edit(fd) {
                var d = this.$commonService.$q.defer(),
                    data = this.$commonService.$http.editUserInfo(fd);
                data.success(function () {
                    d.resolve(true);
                }).error(function () {
                    d.resolve(false);
                });
                return d.promise;
            }
        }, {
            key: 'delete',
            value: function _delete(request) {
                var d = this.$commonService.$q.defer(),
                    data = this.$commonService.$http.deleteUserInfo(request);
                data.success(function () {
                    d.resolve(true);
                }).error(function () {
                    d.resolve(false);
                });
                return d.promise;
            }
        }]);

        return accountEditService;
    }();

    angular.module('App').service('accountEditService', function (commonService, messageService) {
        return new accountEditService(commonService, messageService);
    });
})();

},{}],27:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    'use strict';

    var accountRegisterService = function () {
        function accountRegisterService(commonService, messageService) {
            _classCallCheck(this, accountRegisterService);

            this.message = "";
            this.$messageService = messageService;
            this.$commonService = commonService;
        }

        _createClass(accountRegisterService, [{
            key: "register",
            value: function register(fd) {
                var d = this.$commonService.$q.defer(),
                    data = this.$commonService.$http.putUserInfo(fd);
                data.success(function () {
                    d.resolve("complete");
                }).error(function () {
                    d.resolve("fail");
                });
                return d.promise;
            }
        }]);

        return accountRegisterService;
    }();

    angular.module('App').service('accountRegisterService', function (commonService, messageService) {
        return new accountRegisterService(commonService, messageService);
    });
})();

},{}],28:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {

    'use strict';

    var commonService = function () {
        function commonService($cookies, $state, $q, $timeout, http, cookieService, messageService, pushNotificationService) {
            _classCallCheck(this, commonService);

            this.cashElement($cookies, $state, $q, $timeout, http, cookieService, messageService, pushNotificationService);
        }

        _createClass(commonService, [{
            key: 'cashElement',
            value: function cashElement($cookies, $state, $q, $timeout, http, cookieService, messageService, pushNotificationService) {

                this.$cookies = $cookies;
                this.$state = $state;
                this.$q = $q;
                this.$timeout = $timeout;
                this.$http = http;

                this.$cookieService = cookieService;
                this.$messageService = messageService;
                this.$pushNotificationService = pushNotificationService;

                this.loader__wrap = document.querySelector('.loader__wrap');

                this.projectGrade = ["V0", "V1", "V2", "V3", "V4", "V5", "V6", "V7", "V8", "V9", "V10", "V11", "V12", "V13", "V14", "V15", "V16"];
            }
        }, {
            key: 'spaceReplace',
            value: function spaceReplace(t) {
                var temp = t.trim();
                return temp;
            }
        }, {
            key: 'logout',
            value: function logout() {
                this.$cookies.remove("loginId");
                this.$cookies.remove("loginState");
                this.$cookies.remove("editProject");
                this.$state.go('login');
            }
        }, {
            key: 'clipboard',
            value: function clipboard() {
                this.$pushNotificationService.output(this.$messageService.clipboard_msg_200());
            }
        }, {
            key: 'loader_visible',
            value: function loader_visible() {
                this.loader__wrap.style.display = "block";
            }
        }, {
            key: 'loader_inVisible',
            value: function loader_inVisible() {
                this.loader__wrap.style.display = "none";
            }
        }]);

        return commonService;
    }();

    angular.module('App').factory('commonService', function ($cookies, $state, $q, $timeout, http, cookieService, messageService, pushNotificationService) {
        return new commonService($cookies, $state, $q, $timeout, http, cookieService, messageService, pushNotificationService);
    });
})();

},{}],29:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
  'use strict';

  var cookieService = function () {
    function cookieService($cookies, $state) {
      _classCallCheck(this, cookieService);

      this.cashElement($cookies, $state);
      this.bindEvents();
    }

    _createClass(cookieService, [{
      key: 'cashElement',
      value: function cashElement($cookies, $state) {
        this.$cookies = $cookies;
        this.$state = $state;
      }
    }, {
      key: 'bindEvents',
      value: function bindEvents() {
        var _this = this;

        window.addEventListener("hashchange", function () {
          _this.haveCookieCheck();
        }, false);
      }
    }, {
      key: 'haveCookieCheck',
      value: function haveCookieCheck() {
        if (!this.$cookies.get('loginState') && this.$state.current.name != "accountRegister" && this.$state.current.name != "term") {
          this.$state.go('login');
        } else {
          if (this.$state.current.name == "login") {
            this.$state.go("userInfo", { "userId": this.$cookies.get("loginId") });
          }
        }
      }
    }]);

    return cookieService;
  }();

  angular.module('App').service('cookieService', function ($cookies, $state) {
    return new cookieService($cookies, $state);
  });
})();

},{}],30:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function (root) {
    var createElements = function () {
        /* @ngInject */

        function createElements() {
            _classCallCheck(this, createElements);
        }

        _createClass(createElements, [{
            key: 'cacheElements',
            value: function cacheElements() {
                this.add_btn = document.querySelectorAll('.element-add');
                this.create__imageEditor = document.querySelector('.create__imageEditor');
                this.current_element = 0;
            }
        }, {
            key: 'bindEvents',
            value: function bindEvents() {
                var that = this;
                for (var i = 0; i < this.add_btn.length; i++) {
                    that.add_btn[i].addEventListener('click', function (e) {
                        that.element_add(this);
                    });
                }
            }
        }, {
            key: 'element_delete',
            value: function element_delete(target) {
                target.remove();
            }
        }, {
            key: 'element_add',
            value: function element_add(target) {
                var that = this,
                    setElement = "",
                    markType = target.getAttribute('data-marktype'),
                    colorType = target.getAttribute('data-colortype'),
                    setNum = that.current_element,
                    setElementName = "create__" + markType + "Img" + setNum,
                    itemH = markType.match(/circle/) == null ? "35px" : "112px";
                setElement = document.createElement("div");
                setElement.setAttribute('id', setElementName);
                setElement.setAttribute('class', "create__iconImg create__iconItem--" + target.getAttribute('data-type'));
                setElement.setAttribute('data-src', "../assets/img/create/" + colorType + "/" + markType + ".png");
                setElement.setAttribute('data-x', "0");
                setElement.setAttribute('data-y', "0");
                setElement.setAttribute('data-w', "120");
                setElement.setAttribute('data-h', itemH);
                setElement.style.width = "120px";
                setElement.style.height = itemH;
                setElement.style.top = "0px";
                setElement.style.left = "0px";
                setElement.innerHTML = '\n                <div id="create__' + markType + 'ImgInner' + setNum + '" class="create__iconImgInner">\n                <p class="create__imageBase">\n                    <img src="../assets/img/create/' + colorType + '/' + markType + '.png" alt="" >\n                </p>\n                </div>\n             ';
                that.create__imageEditor.appendChild(setElement);
                this.element_bind_event(setElementName);
                that.current_element++;
            }
        }, {
            key: 'element_bind_event',
            value: function element_bind_event(TARGET) {

                var that = this,
                    D_TAP_EVENT = "";

                D_TAP_EVENT = new Hammer(document.getElementById(TARGET));
                D_TAP_EVENT.on('doubletap', function (ev) {
                    that.element_delete(document.getElementById(TARGET));
                });

                interact('#' + TARGET).draggable({
                    onmove: that.dragMoveListener
                }).resizable({
                    preserveAspectRatio: true,
                    edges: { left: true, right: true, bottom: true, top: true }
                }).on('resizemove', that.resizemove);
            }
        }, {
            key: 'resizemove',
            value: function resizemove(event) {
                var target = event.target,
                    x = parseFloat(target.getAttribute('data-x')) || 0,
                    y = parseFloat(target.getAttribute('data-y')) || 0,
                    w = event.rect.width,
                    h = event.rect.height;

                target.style.width = event.rect.width + 'px';
                target.style.height = event.rect.height + 'px';

                x += event.deltaRect.left;
                y += event.deltaRect.top;

                target.style.left = x + 'px';
                target.style.top = y + 'px';

                // target.style.webkitTransform = target.style.transform =
                //     'translate(' + x + 'px,' + y + 'px)';

                target.setAttribute('data-x', x);
                target.setAttribute('data-y', y);
                target.setAttribute('data-w', w);
                target.setAttribute('data-h', h);
                // target.textContent = Math.round(event.rect.width) + '×' + Math.round(event.rect.height);
            }
        }, {
            key: 'dragMoveListener',
            value: function dragMoveListener(event) {
                var target = event.target,
                    x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                    y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy,
                    w = event.target.style.width.split('px')[0],
                    h = event.target.style.height.split('px')[0];

                // target.style.webkitTransform =
                //     target.style.transform =
                //     'translate(' + x + 'px, ' + y + 'px)';

                target.style.left = x + 'px';
                target.style.top = y + 'px';

                // target.style.webkitTransform =
                //     target.style.transform =
                //     'translate(' + x + 'px, ' + y + 'px)';

                target.setAttribute('data-x', x);
                target.setAttribute('data-y', y);
                target.setAttribute('data-w', w);
                target.setAttribute('data-h', h);
            }
        }]);

        return createElements;
    }();

    angular.module('App').factory('createElements', function () {
        return new createElements();
    });
})(window);

},{}],31:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    'use strict';

    var http = function () {
        function http($http, $httpParamSerializerJQLike) {
            _classCallCheck(this, http);

            this.$http = $http;
            this.$httpParamSerializerJQLike = $httpParamSerializerJQLike;
        }

        _createClass(http, [{
            key: 'e',
            value: function e(option) {
                return this.$http(option);
            }
        }, {
            key: 'auth',
            value: function auth(username, password) {
                return this.$http({
                    method: 'POST',
                    url: '/api/user/login/',
                    data: {
                        "username": username,
                        "password": password
                    }
                });
            }
        }, {
            key: 'editUserInfo',
            value: function editUserInfo(fd) {
                return this.$http({
                    method: 'POST',
                    url: '/api/user/edit/',
                    transformRequest: null,
                    headers: {
                        'Content-Type': undefined
                    },
                    data: fd
                });
            }
        }, {
            key: 'deleteUserInfo',
            value: function deleteUserInfo(request) {
                return this.$http({
                    method: 'DELETE',
                    url: '/api/user/delete/' + request._id
                });
            }
        }, {
            key: 'getUserInfo',
            value: function getUserInfo(username) {
                return this.$http({
                    method: 'POST',
                    url: '/api/user/info/',
                    data: {
                        "username": username
                    }
                });
            }
        }, {
            key: 'putUserInfo',
            value: function putUserInfo(fd) {
                return this.$http({
                    method: 'PUT',
                    url: '/api/user/create/',
                    transformRequest: null,
                    headers: {
                        'Content-Type': undefined
                    },
                    data: fd
                });
            }
        }, {
            key: 'getProjectInfo',
            value: function getProjectInfo(request) {
                return this.$http({
                    method: 'POST',
                    url: '/api/project/',
                    data: request
                });
            }
        }, {
            key: 'editProject',
            value: function editProject(fd) {
                return this.$http({
                    method: 'POST',
                    url: '/api/project/edit/',
                    transformRequest: null,
                    headers: {
                        'Content-Type': undefined
                    },
                    data: fd
                });
            }
        }, {
            key: 'deleteProject',
            value: function deleteProject(request) {
                return this.$http({
                    method: 'DELETE',
                    url: '/api/project/delete/' + request._id
                });
            }
        }, {
            key: 'putProject',
            value: function putProject(fd) {
                return this.$http({
                    method: 'PUT',
                    url: '/api/project/create/',
                    transformRequest: null,
                    headers: {
                        'Content-Type': undefined
                    },
                    data: fd
                });
            }
        }]);

        return http;
    }();

    angular.module('App').factory('http', function ($http, $httpParamSerializerJQLike) {
        return new http($http, $httpParamSerializerJQLike);
    });
})();

},{}],32:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    'use strict';

    var imageGenarator = function () {
        /* @ngInject */

        function imageGenarator() {
            _classCallCheck(this, imageGenarator);
        }

        _createClass(imageGenarator, [{
            key: 'cashElements',
            value: function cashElements() {
                this.item_count = 0;
                this.b = document.body;
                this.items = new Array();
                this.fileInput = document.getElementById('fileInput');
                this.overlay = document.querySelector('.overlay');
                this.create__editorCtrl = document.querySelector('.create__editorCtrl');
                this.create__imageEditor = document.querySelector('.create__imageEditor');
                this.create__registerBtn = document.querySelector('.create__registerBtn');
                this.overlay__resultImg = document.querySelector('.overlay__resultImg');
                this.overlay_close = document.querySelectorAll('.overlay__close');
                this.loader = document.querySelector('.loader__wrap');
            }
        }, {
            key: 'bindEvents',
            value: function bindEvents() {
                var that = this;
                this.fileInput.addEventListener('change', function (e) {
                    that.base_image_set(e);
                });
                this.create__registerBtn.addEventListener('click', function (e) {
                    that.genarateProject();
                });
                for (var i = 0; i < this.overlay_close.length; i++) {
                    this.overlay_close[i].addEventListener('click', function (e) {
                        that.element_reset();
                    });
                }
            }
        }, {
            key: 'base_image_set',
            value: function base_image_set(e) {
                var that = this,
                    img = e.target.files[0],
                    orientation_img = "",
                    compress_img = "";
                // this.loader.style.height = window.height
                this.loader.style.display = "block";
                EXIF.getData(img, function () {
                    orientation_img = img.exifdata.Orientation;
                    compress_img = new MegaPixImage(img);
                    that.create__imageEditor.innerHTML = '<p class="create__imageBase"><img src="" alt="" id="base_img"></p>';
                    compress_img.render(document.getElementById('base_img'), { orientation: orientation_img });
                    document.getElementById('base_img').addEventListener('load', function (e) {
                        that.create__editorCtrl.style.display = "block";
                        that.create__registerBtn.style.display = "table";
                        that.loader.style.display = "none";
                    });
                });
            }
        }, {
            key: 'element_reset',
            value: function element_reset() {
                var deleteTarget = document.querySelectorAll('.create__iconImg');
                for (var i = 0; i < deleteTarget.length; i++) {
                    deleteTarget[i].remove();
                }
                this.overlay.style.display = "none";
            }
        }, {
            key: 'genarateProject',
            value: function genarateProject() {
                var that = this,
                    cnv = document.getElementById('phantom_cnv'),
                    ctx = cnv.getContext("2d"),
                    baseImage = document.getElementById('base_img'),
                    imageElement = new Image(baseImage.offsetWidth * 0.5, baseImage.offsetHeight * 0.5),
                    iconImages = document.querySelectorAll('.create__iconImg'),
                    iconImagesArr = new Array(),
                    loadcount = 1;

                // this.loader.style.height = window.height
                this.loader.style.display = "block";

                for (var i = 0; i < iconImages.length; i++) {
                    iconImagesArr.push(iconImages[i]);
                }

                cnv.width = baseImage.offsetWidth;
                cnv.height = baseImage.offsetHeight;
                ctx.fillRect(0, 0, baseImage.offsetWidth, baseImage.offsetHeight);

                imageElement.src = baseImage.getAttribute('src');
                imageElement.onload = function () {
                    ctx.drawImage(imageElement, 0, 0, baseImage.offsetWidth, baseImage.offsetHeight);
                    if (iconImages.length == 0) {
                        overlayDisplay();
                    } else {
                        iconImagesArr.forEach(function (val, index, ar) {
                            var tmp_parts_img = new Image(val.getAttribute('data-w'), val.getAttribute('data-h'));
                            tmp_parts_img.src = val.getAttribute('data-src') + "?" + new Date().getTime();
                            elementImgCanvasSet(tmp_parts_img, val);
                        });
                    }
                };

                function elementImgCanvasSet(t, v) {
                    t.onload = function (e) {
                        loadcount++;
                        ctx.drawImage(t, v.getAttribute('data-x'), v.getAttribute('data-y'), v.getAttribute('data-w'), v.getAttribute('data-h'));
                        if (iconImages.length <= loadcount) {
                            overlayDisplay();
                        }
                    };
                }

                function overlayDisplay() {
                    console.log(that.b);
                    Velocity(that.b, 'scroll', {
                        duration: 450,
                        easing: 'easeInOutCubic',
                        complete: function complete() {
                            that.loader.style.display = "none";
                            that.overlay__resultImg.style.width = baseImage.offsetWidth;
                            that.overlay__resultImg.style.height = baseImage.offsetHeight;
                            that.overlay__resultImg.setAttribute('src', cnv.toDataURL("image/png"));
                            that.overlay__resultImg.style.display = "block";
                            that.overlay.style.display = "block";
                        }
                    });
                }
            }
        }]);

        return imageGenarator;
    }();

    angular.module('App').factory('imageGenarator', function () {
        return new imageGenarator();
    });
})();

},{}],33:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    'use strict';

    var loginService = function () {
        function loginService(commonService, messageService) {
            _classCallCheck(this, loginService);

            this.$commonService = commonService;
            this.$messageService = messageService;
            this.message = "";
        }

        _createClass(loginService, [{
            key: "login",
            value: function login(username, password) {
                var _this = this;

                var d = this.$commonService.$q.defer(),
                    data = this.$commonService.$http.auth(username, password);
                data.success(function (response) {
                    if (response.username == username && response.password == password) {
                        _this.$commonService.$cookieService.$cookies.put("loginState", true, ["expires", 7]);
                        _this.$commonService.$cookieService.$cookies.put("loginId", username, ["expires", 7]);
                        _this.$commonService.$state.go("userInfo", { "userId": username });
                    } else {
                        _this.message = _this.$messageService.login_msg_400();
                    }
                    d.resolve(_this.message);
                }).error(function (response) {
                    _this.message = _this.$messageService.login_msg_400();
                    d.resolve(_this.message);
                });
                return d.promise;
            }
        }]);

        return loginService;
    }();

    angular.module('App').service('loginService', function (commonService, messageService) {
        return new loginService(commonService, messageService);
    });
})();

},{}],34:[function(require,module,exports){
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    'use strict';

    var messageService = function () {
        function messageService() {
            _classCallCheck(this, messageService);
        }

        _createClass(messageService, [{
            key: "function_interruption_msg",
            value: function function_interruption_msg() {
                return "処理を中断しました。";
            }
        }, {
            key: "clipboard_msg_200",
            value: function clipboard_msg_200() {
                return "クリップボードにURLをコピーしました。";
            }
        }, {
            key: "login_msg_200",
            value: function login_msg_200() {
                return "ログインが完了しました。";
            }
        }, {
            key: "login_msg_400",
            value: function login_msg_400() {
                return "入力されたユーザーネームはアカウントと一致しません。ユーザーネームをご確認の上、もう一度実行してください。";
            }
        }, {
            key: "login_msg_503",
            value: function login_msg_503() {
                return "入力されたユーザーネームはアカウントと一致しません。ユーザーネームをご確認の上、もう一度実行してください。";
            }
        }, {
            key: "projectRegister_msg_200",
            value: function projectRegister_msg_200() {
                return "プロジェクトの作成が完了しました。";
            }
        }, {
            key: "projectRegister_msg_400",
            value: function projectRegister_msg_400() {
                return "プロジェクトの作成に失敗しました。";
            }
        }, {
            key: "projectDelete_msg_200",
            value: function projectDelete_msg_200() {
                return "プロジェクトの削除が完了しました。";
            }
        }, {
            key: "projectDelete_msg_400",
            value: function projectDelete_msg_400() {
                return "プロジェクトの削除に失敗しました。";
            }
        }, {
            key: "projectEdit_msg_200",
            value: function projectEdit_msg_200() {
                return "プロジェクトの修正が完了しました。";
            }
        }, {
            key: "projectEdit_msg_400",
            value: function projectEdit_msg_400() {
                return "プロジェクトの修正に失敗しました。";
            }
        }, {
            key: "accoutResiger_msg_200",
            value: function accoutResiger_msg_200() {
                return "アカウントの作成が完了しました。";
            }
        }, {
            key: "accoutResiger_msg_400",
            value: function accoutResiger_msg_400() {
                return "入力されたユーザーネーム、メールアドレスはすでに使用されています。";
            }
        }, {
            key: "accoutEdit_msg_200",
            value: function accoutEdit_msg_200() {
                return "アカウントの修正が完了しました。";
            }
        }, {
            key: "accoutEdit_msg_400",
            value: function accoutEdit_msg_400() {
                return "アカウントの修正に失敗しました。";
            }
        }, {
            key: "accountDelete_msg_200",
            value: function accountDelete_msg_200() {
                return "アカウントの削除が完了しました。";
            }
        }, {
            key: "accountDelete_msg_400",
            value: function accountDelete_msg_400() {
                return "アカウントの削除に失敗しました。";
            }
        }]);

        return messageService;
    }();

    angular.module('App').service('messageService', function () {
        return new messageService();
    });
})();

},{}],35:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    'use strict';

    var projectEditService = function () {
        function projectEditService(commonService, messageService) {
            _classCallCheck(this, projectEditService);

            this.$messageService = messageService;
            this.$commonService = commonService;
        }

        _createClass(projectEditService, [{
            key: 'getProjectInfo',
            value: function getProjectInfo(request) {
                var d = this.$commonService.$q.defer(),
                    data = this.$commonService.$http.getProjectInfo(request);
                data.success(function (response) {
                    d.resolve(response);
                }).error(function () {
                    d.resolve([]);
                });
                return d.promise;
            }
        }, {
            key: 'edit',
            value: function edit(fd) {
                var d = this.$commonService.$q.defer(),
                    data = this.$commonService.$http.editProject(fd);
                data.success(function () {
                    d.resolve(true);
                }).error(function () {
                    d.resolve(false);
                });
                return d.promise;
            }
        }, {
            key: 'delete',
            value: function _delete(request) {
                var d = this.$commonService.$q.defer(),
                    data = this.$commonService.$http.deleteProject(request);
                data.success(function () {
                    d.resolve(true);
                }).error(function (response) {
                    d.resolve(false);
                });
                return d.promise;
            }
        }]);

        return projectEditService;
    }();

    angular.module('App').service('projectEditService', function (commonService, messageService) {
        return new projectEditService(commonService, messageService);
    });
})();

},{}],36:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    'use strict';

    var projectRegisterService = function () {
        function projectRegisterService(commonService, messageService) {
            _classCallCheck(this, projectRegisterService);

            this.$messageService = messageService;
            this.$commonService = commonService;
        }

        _createClass(projectRegisterService, [{
            key: 'register',
            value: function register(fd) {
                var d = this.$commonService.$q.defer(),
                    data = this.$commonService.$http.putProject(fd);
                data.success(function (response) {
                    d.resolve(true);
                }).error(function (response) {
                    d.resolve(false);
                });
                return d.promise;
            }
        }]);

        return projectRegisterService;
    }();

    angular.module('App').service('projectRegisterService', function (commonService, messageService) {
        return new projectRegisterService(commonService, messageService);
    });
})();

},{}],37:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    'use strict';

    var pushNotificationService = function () {
        function pushNotificationService($timeout) {
            _classCallCheck(this, pushNotificationService);

            this.cashElement($timeout);
        }

        _createClass(pushNotificationService, [{
            key: 'cashElement',
            value: function cashElement($timeout) {
                this.$timeout = $timeout;
                this.message = "";
                this.display = false;
            }
        }, {
            key: 'output',
            value: function output(msg) {
                var _this = this;

                this.message = msg;
                this.display = true;
                this.$timeout(function () {
                    _this.display = false;
                }, 5000);
            }
        }]);

        return pushNotificationService;
    }();

    angular.module('App').factory('pushNotificationService', function ($timeout) {
        return new pushNotificationService($timeout);
    });
})();

},{}],38:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    'use strict';

    var streamService = function () {
        function streamService(commonService, messageService) {
            _classCallCheck(this, streamService);

            this.$messageService = messageService;
            this.$commonService = commonService;
        }

        _createClass(streamService, [{
            key: 'getProjectInfo',
            value: function getProjectInfo() {
                var d = this.$commonService.$q.defer(),
                    data = this.$commonService.$http.getProjectInfo();
                data.success(function (response) {
                    d.resolve(response);
                }).error(function (response) {
                    d.resolve(response);
                });
                return d.promise;
            }
        }]);

        return streamService;
    }();

    angular.module('App').service('streamService', function (commonService, messageService) {
        return new streamService(commonService, messageService);
    });
})();

},{}],39:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
    'use strict';

    var userService = function () {
        function userService(commonService, messageService) {
            _classCallCheck(this, userService);

            this.$messageService = messageService;
            this.$commonService = commonService;
        }

        _createClass(userService, [{
            key: 'getUserInfo',
            value: function getUserInfo(username) {
                var d = this.$commonService.$q.defer(),
                    data = this.$commonService.$http.getUserInfo(username);
                data.success(function (response) {
                    d.resolve(response);
                }).error(function (response) {
                    d.resolve(response);
                });
                return d.promise;
            }
        }, {
            key: 'getProjectInfo',
            value: function getProjectInfo(request) {
                var d = this.$commonService.$q.defer(),
                    data = this.$commonService.$http.getProjectInfo(request);
                data.success(function (response) {
                    d.resolve(response);
                }).error(function () {
                    d.resolve([]);
                });
                return d.promise;
            }
        }]);

        return userService;
    }();

    angular.module('App').service('userService', function (commonService, messageService) {
        return new userService(commonService, messageService);
    });
})();

},{}],40:[function(require,module,exports){
"use strict";

module.exports.tpl = "\n                <div class=\"accountEdit\">\n                    <form name=\"accountEditForm\" class=\"accountEdit-form\" id=\"accountEdit-form\" enctype=\"multipart/form-data\">\n                        <div class=\"accountEdit-form__item\">\n                            <span>プロフィール写真：</span>\n                            <span><img src=\"{{accountEdit.iconImg}}\" alt=\"\" width=\"200px\" height=\"auto\"></span>\n                            <input type=\"file\" name=\"file[]\" id=\"file\">\n                        </div>\n                        <div class=\"accountEdit-form__item\">\n                            <span>パスワード：</span>\n                            <input type=\"password\" name=\"password\" placeholder=\"password\" ng-required=\"true\" ng-model=\"accountEdit.password\">\n                        </div>                        \n                        <div ng-show=\"accountEditForm.$submitted || accountEditForm.password.$touched\">\n                            <p class=\"errorMessage\" ng-show=\"accountEditForm.password.$error.required\">パスワードは必須です。</p>\n                        </div>\n                        <div class=\"accountEdit-form__item\">\n                            <span>メールアドレス：</span>\n                            <input type=\"email\" name=\"mailAddress\" placeholder=\"hoge@hoge.com\" ng-required=\"true\" ng-model=\"accountEdit.mailAddress\">\n                        </div>\n                        <div ng-show=\"accountEditForm.$submitted || accountEditForm.mailAddress.$touched\">\n                            <p class=\"errorMessage\" ng-show=\"accountEditForm.mailAddress.$error.required\">メールアドレスは必須です。</p>\n                            <p class=\"errorMessage\" ng-show=\"accountEditForm.mailAddress.$error.email\">メールアドレスを入力してください。</p>                            \n                        </div>                        \n                        <div class=\"accountEdit-form__item\">\n                            <span>ウェブサイト：</span>\n                            <input type=\"text\" name=\"website\" placeholder=\"http://hogehoge.com/\" ng-model=\"accountEdit.hpUrl\">\n                        </div>\n                        <div class=\"accountEdit-form__item\">                            \n                            <span>自己紹介：</span>\n                            <textarea name=\"selfIntroduce\" id=\"\" cols=\"30\" rows=\"10\" placeholder=\"HogeHoge\" ng-model=\"accountEdit.introduction\"></textarea>\n                        </div>\n                        <div class=\"accountEdit__message\">\n                            {{accountEdit.message}}\n                        </div>                \n                        <div class=\"accountEdit-form__item ranks_type_parallel__items\">   \n                            <div class=\"ranks_type_parallel__item button_type_circle\" ng-click=\"accountEdit.edit()\">\n                                <a >\n                                    <span>編集</span>\n                                </a>\n                            </div>\n                            <div class=\"ranks_type_parallel__item button_type_circle\" ng-click=\"accountEdit.delete()\">\n                                <a>\n                                    <span>削除</span>\n                                </a>\n                            </div>      \n                        </div> \n                    </form>\n                </div>\n";

},{}],41:[function(require,module,exports){
"use strict";

module.exports.tpl = "\n            <div class=\"accountRegister\">                     \n                <form ng-submit=\"accountRegisterForm.$valid && accountRegister.register()\" novalidate id=\"accountRegister-form\" class=\"accountRegister-form form\" name=\"accountRegisterForm\">\n                    <div class=\"accountRegister-form__item form__item\">\n                        <input type=\"email\" name=\"mailaddress\" ng-required=\"true\" placeholder=\"メールアドレス\" ng-model=\"accountRegister.mailaddress\">\n                    </div>\n                    <div ng-show=\"accountRegisterForm.$submitted || accountRegisterForm.mailaddress.$touched\">\n                        <p class=\"errorMessage\" ng-show=\"accountRegisterForm.mailaddress.$error.required\">メールアドレスを入力してください</p>\n                    </div>\n                    <div ng-show=\"accountRegisterForm.$submitted || accountRegisterForm.mailaddress.$touched\">\n                        <p class=\"errorMessage\" ng-show=\"accountRegisterForm.mailaddress.$error.email\">入力形式に誤りがあります。</p>\n                    </div>                    \n                    <div class=\"accountRegister-form__item form__item\">\n                        <input type=\"text\" name=\"username\" ng-required=\"true\" placeholder=\"ユーザネーム\" ng-model=\"accountRegister.username\" maxlength=\"30\">\n                    </div>\n                    <div ng-show=\"accountRegisterForm.$submitted || accountRegisterForm.username.$touched\">\n                        <p class=\"errorMessage\" ng-show=\"accountRegisterForm.username.$error.required\">ユーザネームを入力してください</p>\n                    </div>\n                    <div class=\"accountRegister-form__item form__item\">\n                        <input type=\"password\" name=\"password\" ng-required=\"true\" placeholder=\"パスワード\" ng-model=\"accountRegister.password\" >\n                    </div>\n                    <div ng-show=\"accountRegisterForm.$submitted || accountRegisterForm.password.$touched\">\n                        <p class=\"errorMessage\" ng-show=\"accountRegisterForm.password.$error.required\">パスワードを入力してください</p>\n                    </div>\n                    <div class=\"accountRegister-form__item form__item\">\n                        <input type=\"checkbox\" name=\"term\" ng-required=\"true\" ng-model=\"accountRegister.term\"><a ui-sref=\"term\">利用規約</a>に同意する\n                    </div>\n                    <div ng-show=\"accountRegisterForm.$submitted || accountRegisterForm.term.$touched\">\n                        <p class=\"errorMessage\" ng-show=\"accountRegisterForm.term.$error.required\">利用規約に同意できない場合はアカウント作成できません。</p>\n                    </div> \n                    <div class=\"accountRegister-form__item form__item\">\n                        <div class=\"accountRegister-form__submit button_type_square\">\n                            <button>\n                                <span>アカウント作成</span>\n                            </button>\n                        </div>\n                    </div>\n                    <div class=\"accountRegister-form__item form__item\">\n                        <div class=\"accountRegister-form__submit button_type_square\">\n                            <a ui-sref=\"login\">\n                                <span>戻る</span>\n                            </a>\n                        </div>\n                    </div>                    \n                </form>\n                <div class=\"accountRegister__message\">\n                    {{accountRegister.message}}\n                </div>                \n            </div>\n";

},{}],42:[function(require,module,exports){
"use strict";

module.exports.tpl = "\n            <ul class=\"gnav__items\">\n                <li class=\"gnav__item\">\n                    <a ui-sref=\"stream\">\n                        <img src=\"assets/img/common/icon/folder.png\" width=\"25\" height=\"25\" alt=\"explore\">\n                    </a>\n                </li>               \n                <li class=\"gnav__item\">\n                    <a ui-sref=\"projectMaker\">\n                        <img src=\"assets/img/common/icon/edit.png\" width=\"25\" height=\"25\" alt=\"create\">\n                    </a>                \n                </li>\n                <li class=\"gnav__item\">\n                    <a ui-sref=\"projectRegister\">\n                        <img src=\"assets/img/common/icon/upload-1.png\" width=\"25\" height=\"25\" alt=\"add\">\n                    </a>\n                </li>                \n\n                <li class=\"gnav__item\">\n                    <a ui-sref=\"userInfo({userId:'{{gnav.username}}'})\">                \n                        <img src=\"assets/img/common/icon/avatar.png\" width=\"25\" height=\"25\" alt=\"home\">\n                    </a>\n                </li>\n                <li class=\"gnav__item\">\n                    <a ui-sref=\"accountEdit\"> \n                        <img src=\"assets/img/common/icon/settings.png\" width=\"25\" height=\"25\" alt=\"\">\n                    </a>\n                </li>\n                <li class=\"gnav__item\">\n                    <a ng-click=\"gnav.$commonService.logout()\"> \n                        <img src=\"assets/img/common/icon/logout.png\" width=\"25\" height=\"25\" alt=\"\">\n                    </a>\n                </li>\n            </ul>\n";

},{}],43:[function(require,module,exports){
"use strict";

module.exports.tpl = "\n            <div class=\"login\">\n                <form novalidate class=\"login-form\" name=\"loginForm\">\n                    <div class=\"login-form__item\">\n                        <input type=\"text\" name=\"username\" ng-model=\"login.username\" ng-required=\"true\" ng-maxlength=\"30\" placeholder=\"ユーザネーム\">\n                    </div>\n                    \n                    <div ng-show=\"loginForm.$submitted || loginForm.username.$touched\">\n                        <p class=\"errorMessage\" ng-show=\"loginForm.username.$error.required\">ユーザネームを入力してください</p>\n                    </div>\n\n                    <div class=\"login-form__item\">\n                        <input type=\"password\" name=\"password\" ng-model=\"login.password\" ng-required=\"true\" placeholder=\"パスワード\">\n                    </div>\n\n                    <div ng-show=\"loginForm.$submitted || loginForm.password.$touched\">\n                        <p class=\"errorMessage\" ng-show=\"loginForm.password.$error.required\">パスワードを入力してください</p>\n                    </div>\n\n                    <div class=\"login-form__item login-form__submit button_type_square\">\n                        <button ng-click=\"login.login()\">ログイン</button>\n                    </div>\n                </form>\n                <div class=\"login__createUser button_type_square\">\n                    <a ui-sref=\"accountRegister\">\n                        <span>アカウント作成</span>\n                    </a>\n                </div>\n                <div class=\"login__message\">\n                    <p>{{login.message}}<p>\n                    <div ng-show=\"loginForm.$submitted\">\n                        <p ng-show=\"loginForm.username.$error.required || loginForm.password.$error.required\">必須項目が入力されていません。</p>\n                    </div>\n                </div>\n            </div>\n";

},{}],44:[function(require,module,exports){
"use strict";

module.exports.tpl = "\n                <div class=\"projectEdit\">\n                    <div class=\"projectEdit-preview\">\n                        <img src=\"{{projectEdit.projectImg}}\" alt=\"\">\n                    </div>\n                    <form name=\"projectEditForm\" id=\"projectEdit-form\" class=\"projectEdit-form\" enctype=\"multipart/form-data\">\n                        <input type=\"hidden\" ng-model=\"projectEdit.projectId\" value=\"{{projectEdit.projectId}}\" name=\"projectId\">\n                        <div class=\"projectEdit-form__item\">\n                            <span>課題の名前：</span>\n                            <input type=\"text\" name=\"projectName\" ng-model=\"projectEdit.projectName\">\n                        </div>\n                        <div class=\"projectEdit-form__item\">\n                            <span>課題のある場所：</span>\n                            <input type=\"text\" name=\"projectPlace\" ng-model=\"projectEdit.projectPlace\">\n                        </div>\n                        <div class=\"projectEdit-form__item\">\n                            <span>グレード：</span>\n                            <select name=\"projectGrade\" ng-required=\"true\" ng-model=\"projectEdit.projectGrade\" ng-options=\"grade for grade in projectEdit.projectGradeList\">\n                                <option value=\"\">グレードを選択してください</option>\n                            </select>\n                        </div>\n                        <div class=\"projectRegister-form__item form__item item_type_radio\">\n                            <span>プライバシー設定：</span>\n                            公開：<input type=\"radio\" ng-model=\"projectEdit.isPublic\" value=\"true\" name=\"isPublic\">&nbsp;\n                            自分のみ：<input type=\"radio\" ng-model=\"projectEdit.isPublic\" value=\"false\" name=\"isPublic\">\n                        </div>\n                        <div class=\"errorMessage\">\n                            {{projectEdit.message}}\n                        </div>\n                        <div class=\"projectEdit-form__item ranks_type_parallel__items\">   \n                            <div class=\"projectEdit-form__submit ranks_type_parallel__item button_type_circle\" ng-click=\"projectEdit.edit()\">\n                                <a>\n                                    <span>編集</span>\n                                </a>\n                            </div>\n                            <div class=\"projectEdit-form__submit ranks_type_parallel__item button_type_circle\" ng-click=\"projectEdit.delete()\">\n                                <a>\n                                    <span>削除</span>\n                                </a>\n                            </div>      \n                        </div>              \n                    </form>\n                </div>\n";

// <div class="projectEdit-form__item">
//     <span>課題の写真：</span>
//     <input type="file" name="file[]" id="file">
// </div>

},{}],45:[function(require,module,exports){
"use strict";

module.exports.tpl = "\n        <div class=\"create\">\n                <div class=\"create__item\">\n                    <div class=\"create__imageInput\">\n                        <p>\n                            クライミングウォールの写真を選択してください。\n                        </p>\n                        <form enctype=\"multipart/form-data\" name=\"projectcreate__form\" id=\"projectcreate__form\">\n                            <div>\n                                ファイルを選択\n                                <input type=\"file\" name=\"file[]\" multiple=\"\" id=\"fileInput\">\n                            </div>\n                        </form>\n                    </div>\n                    <div class=\"create__image\">\n                        <div class=\"create__editorCtrl\">\n                            <ul class=\"create__iconList\">\n\n                                <li class=\"create__iconItem\">\n                                    <a class=\"element-add\" data-colortype=\"r\" data-marktype=\"circle__s\"><img src=\"/assets/img/create/r/circle__s.png\" alt=\"\" ></a>\n                                </li>\n                                <li class=\"create__iconItem\">\n                                    <a class=\"element-add\" data-colortype=\"g\" data-marktype=\"circle__s\"><img src=\"/assets/img/create/g/circle__s.png\" alt=\"\" ></a>\n                                </li>\n                                <li class=\"create__iconItem\">\n                                    <a class=\"element-add\" data-colortype=\"b\" data-marktype=\"circle__s\"><img src=\"/assets/img/create/b/circle__s.png\" alt=\"\" ></a>\n                                </li>\n\n                                <li class=\"create__iconItem\">\n                                    <a class=\"element-add\" data-colortype=\"r\" data-marktype=\"circle__r\"><img src=\"/assets/img/create/r/circle__r.png\"  alt=\"\"></a>\n                                </li>\n                                <li class=\"create__iconItem\">\n                                    <a class=\"element-add\" data-colortype=\"g\" data-marktype=\"circle__r\"><img src=\"/assets/img/create/g/circle__r.png\"  alt=\"\"></a>\n                                </li>\n                                <li class=\"create__iconItem\">\n                                    <a class=\"element-add\" data-colortype=\"b\" data-marktype=\"circle__r\"><img src=\"/assets/img/create/b/circle__r.png\"  alt=\"\"></a>\n                                </li>\n                                \n                                <li class=\"create__iconItem\">\n                                    <a class=\"element-add\" data-colortype=\"r\" data-marktype=\"circle__g\"><img src=\"/assets/img/create/r/circle__g.png\" alt=\"\"></a>\n                                </li>\n                                <li class=\"create__iconItem\">\n                                    <a class=\"element-add\" data-colortype=\"g\" data-marktype=\"circle__g\"><img src=\"/assets/img/create/g/circle__g.png\" alt=\"\"></a>\n                                </li>\n                                <li class=\"create__iconItem\">\n                                    <a class=\"element-add\" data-colortype=\"b\" data-marktype=\"circle__g\"><img src=\"/assets/img/create/b/circle__g.png\" alt=\"\"></a>\n                                </li>\n                                \n                                <li class=\"create__iconItem type_of_kante\">\n                                    <a class=\"element-add\" data-colortype=\"r\" data-marktype=\"kante\"><img src=\"/assets/img/create/r/kante.png\"  alt=\"\"></a>\n                                </li>                                \n                                <li class=\"create__iconItem type_of_kante\">\n                                    <a class=\"element-add\" data-colortype=\"g\" data-marktype=\"kante\"><img src=\"/assets/img/create/g/kante.png\"  alt=\"\"></a>\n                                </li>                                \n                                <li class=\"create__iconItem type_of_kante\">\n                                    <a class=\"element-add\" data-colortype=\"b\" data-marktype=\"kante\"><img src=\"/assets/img/create/b/kante.png\"  alt=\"\"></a>\n                                </li>\n\n                                <li class=\"create__iconItem\">\n                                    <a class=\"element-add\" data-colortype=\"r\" data-marktype=\"bote\"><img src=\"/assets/img/create/r/bote.png\"  alt=\"\"></a>\n                                </li>\n                                <li class=\"create__iconItem\">\n                                    <a class=\"element-add\" data-colortype=\"g\" data-marktype=\"bote\"><img src=\"/assets/img/create/g/bote.png\"  alt=\"\"></a>\n                                </li>\n                                <li class=\"create__iconItem\">\n                                    <a class=\"element-add\" data-colortype=\"b\" data-marktype=\"bote\"><img src=\"/assets/img/create/b/bote.png\"  alt=\"\"></a>\n                                </li>                                                                \n                            </ul>\n                        </div>\n                        <div class=\"create__imageEditor\">\n                            <p class=\"create__imageBase\"><img src=\"/assets/img/create/base.jpg\" alt=\"\" id=\"base_img\"></p>\n                        </div>\n                    </div>\n                    <div class=\"create__btns cf\">\n                        <div class=\"create__registerBtn\">\n                            <p class=\"create__registerLink\">作成する</p>\n                        </div>\n                    </div>\n                </div>\n        </div>\n        <div class=\"overlay\">\n            <div class=\"overlay__bg overlay__close\"></div>\n            <p class=\"overlay__closeBtn overlay__close\">\n                <img src=\"assets/img/common/icon/cancel.png\" width=\"50\" height=\"50\" alt=\"\">\n            </p>\n            <div class=\"overlay__result\">\n                <img src=\"\" class=\"overlay__resultImg\"></img>\n                <canvas id=\"phantom_cnv\"></canvas>\n            </div>\n        </div>\n    ";

},{}],46:[function(require,module,exports){
"use strict";

module.exports.tpl = "\n                <div class=\"projectRegister\">\n                    <form ng-submit=\"projectRegisterForm.$valid && projectRegister.register()\" novalidate name=\"projectRegisterForm\" class=\"projectRegister-form form\" id=\"projectRegister-form\" enctype=\"multipart/form-data\">\n                        <div class=\"projectRegister-form__item form__item\">\n                            <span>課題の写真：</span>\n                            <input type=\"file\" name=\"file[]\" id=\"file\" multiple=\"\" ng-required=\"true\">\n                        </div>\n                        <div class=\"projectRegister-form__item form__item\">\n                            <span>課題名：</span>\n                            <input type=\"text\" name=\"projectName\" ng-required=\"true\" ng-model=\"projectRegister.projectName\">\n                        </div>\n                        <div ng-show=\"projectRegisterForm.$submitted || projectRegisterForm.projectName.$touched\">\n                            <p class=\"errorMessage\" ng-show=\"projectRegisterForm.projectName.$error.required\">課題名は必須です。</p>\n                        </div>                        \n                        <div class=\"projectRegister-form__item form__item\">\n                            <span>場所：</span>\n                            <input type=\"text\" name=\"projectPlace\" ng-required=\"true\" ng-model=\"projectRegister.projectPlace\">\n                        </div>\n                        <div ng-show=\"projectRegisterForm.$submitted || projectRegisterForm.projectPlace.$touched\">\n                            <p class=\"errorMessage\" ng-show=\"projectRegisterForm.projectPlace.$error.required\">場所は必須です。</p>\n                        </div>\n                        <div class=\"projectRegister-form__item form__item\">\n                            <span>グレード：</span>\n                            <select name=\"projectGrade\" ng-required=\"true\" ng-model=\"projectRegister.projectGrade\" ng-options=\"grade for grade in projectRegister.projectGradeList\">\n                                <option value=\"\">グレードを選択してください</option>\n                            </select>\n                        </div>\n                        <div ng-show=\"projectRegisterForm.$submitted || projectRegisterForm.projectGrade.$touched\">\n                            <p class=\"errorMessage\" ng-show=\"projectRegisterForm.projectGrade.$error.required\">グレードは必須です。</p>\n                        </div>                                      \n                        <div class=\"projectRegister-form__item form__item item_type_radio\">\n                            <span>プライバシー設定：</span>\n                            公開：<input type=\"radio\" ng-model=\"projectRegister.isPublic\" value=\"true\" name=\"isPublic\">&nbsp;\n                            自分のみ：<input type=\"radio\" ng-model=\"projectRegister.isPublic\" value=\"false\" name=\"isPublic\">\n                        </div>\n                        <div class=\"errorMessage\">\n                        {{projectRegister.message}}\n                        </div>\n                        <div class=\"projectRegister-form__item projectRegister-form__submit button_type_circle\">\n                            <button>\n                                <span>作成</span>\n                            </button>\n                        </div>\n                    </form>\n                </div>\n";

},{}],47:[function(require,module,exports){
"use strict";

module.exports.tpl = "\n        <div class=\"pushNotification\" ng-if=\"pushNotification.$commonService.$pushNotificationService.display\" ng-click=\"pushNotification.$commonService.$pushNotificationService.display=false\"\n            <span>{{ pushNotification.$commonService.$pushNotificationService.message }}</span>\n        </div>\n     \n";

},{}],48:[function(require,module,exports){
"use strict";

module.exports.tpl = "\n                <div class=\"stream__search\">\n                    <form action=\"\" class=\"stream__form\">\n                        <div class=\"project__searchInput\">\n                            <input type=\"text\" placeholder=\"検索\" ng-model=\"searchText\">\n                        </div>\n                    </form>\n                </div>\n                <ul class=\"stream__items\">\n                    <li class=\"stream__item\" ng-repeat=\"(k,v) in stream.projectList | filter:searchText\" ng-show=\"v.isPublic || stream.username != v.projectAuthor\">\n                        <header class=\"project__head cf\">\n                            <div class=\"project__name\">\n                                {{v.projectName}}\n                            </div>\n                            <div class=\"project__place\">\n                                <span class=\"project__placeIcon\"><img src=\"assets/img/common/icon/placeholder.png\" width=\"13\" height=\"13\" alt=\"\"></span>{{v.projectPlace}}\n                            </div>\n                            <div class=\"project__grade\">\n                                <span>{{v.projectGrade}}</span>\n                            </div>\n                            <div class=\"project__author\">\n                                <span class=\"project__authorIcon\"><img src=\"assets/img/common/icon/avatar.png\" width=\"13\" height=\"13\" alt=\"\"></span><a ui-sref=\"userInfo({userId:'{{v.projectAuthor}}'})\">{{v.projectAuthor}}</a>\n                            </div>\n                        </header>\n                        <figure class=\"project__img\">\n                            <img src=\"{{v.projectImg}}\" alt=\"\">\n                        </figure>\n                        <footer class=\"project__foot cf\">\n                            <ul class=\"project-action__items cf\">\n                              <li class=\"project-action__item\">\n                               <div class=\"project__edit\" ng-show=\"(stream.username == v.projectAuthor)\">\n                                    <a ui-sref=\"projectEdit({projectId:'{{v._id}}'})\" >\n                                       <span class=\"project__shareIcon\">\n                                           <img src=\"assets/img/common/icon/edit.png\" width=\"20\" height=\"20\" alt=\"\">\n                                       </span>\n                                    </a>\n                               </div>                                \n                              </li>\n                              <li class=\"project-action__item\">\n                               <div class=\"project__share\">\n                                    <a class=\"btn\" ngclipboard data-clipboard-text=\"https://this-is-open-project.herokuapp.com/project/{{v._id}}\" ng-click=\"stream.$commonService.clipboard()\">\n                                       <span class=\"project__shareIcon\">\n                                           <img src=\"assets/img/common/icon/link.png\" width=\"20\" height=\"20\" alt=\"\">\n                                       </span>\n                                    </a>\n                               </div>                           \n                              </li>  \n                            </ul>\n                        </footer>\n                    </li>\n                </ul>\n";

},{}],49:[function(require,module,exports){
"use strict";

module.exports.tpl = "\n                <div class=\"term\">\n                    <div class=\"term__globalTtl\">\n                        利用規約\n                    </div>\n                    <div class=\"term__lead\">\n                        この利用規約（以下，「本規約」といいます。）は，Open Project（以下，「当社」といいます。）がこのウェブサイト上で提供するサービス（以下，「本サービス」といいます。）の利用条件を定めるものです。登録ユーザーの皆さま（以下，「ユーザー」といいます。）には，本規約に従って，本サービスをご利用いただきます。\n                    </div>\n                    <ul class=\"term__items\">\n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第1条（適用）\n                            </div>\n                            <div class=\"term__desc\">\n                                本規約は，ユーザーと当社との間の本サービスの利用に関わる一切の関係に適用されるものとします。\n                            </div>\n                        </li>\n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第2条（利用登録）\n                            </div>\n                            <div class=\"term__desc\">\n                                1.登録希望者が当社の定める方法によって利用登録を申請し，当社がこれを承認することによって，利用登録が完了するものとします。\n                            </div>\n                            <div class=\"term__desc\">\n                                2.当社は，利用登録の申請者に以下の事由があると判断した場合，利用登録の申請を承認しないことがあり，その理由については一切の開示義務を負わないものとします。\n                            </div>\n                            <ul class=\"term__descExample-items\">\n                                <li class=\"term__descExample-item\">\n                                    （1）利用登録の申請に際して虚偽の事項を届け出た場合\n                                </li>\n                                <li class=\"term__descExample-item\">\n                                    （2）本規約に違反したことがある者からの申請である場合\n                                </li>\n                                <li class=\"term__descExample-item\">\n                                    （3）その他，当社が利用登録を相当でないと判断した場合\n                                </li>\n                            </ul>\n                        </li>   \n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第3条（ユーザーIDおよびパスワードの管理）\n                            </div>\n                            <div class=\"term__desc\">\n                                1.ユーザーは，自己の責任において，本サービスのユーザーIDおよびパスワードを管理するものとします。\n                            </div>\n                            <div class=\"term__desc\">\n                                2.ユーザーは，いかなる場合にも，ユーザーIDおよびパスワードを第三者に譲渡または貸与することはできません。当社は，ユーザーIDとパスワードの組み合わせが登録情報と一致してログインされた場合には，そのユーザーIDを登録しているユーザー自身による利用とみなします。\n                            </div>\n                        </li>\n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第4条（利用料金および支払方法）\n                            </div>\n                            <div class=\"term__desc\">\n                                1.ユーザーは，本サービス利用の対価として，当社が別途定め，本ウェブサイトに表示する利用料金を，当社が指定する方法により支払うものとします。\n                            </div>\n                            <div class=\"term__desc\">\n                                2.ユーザーが利用料金の支払を遅滞した場合には，ユーザーは年１４．６％の割合による遅延損害金を支払うものとします。\n                            </div>\n                        </li>\n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第5条（禁止事項）\n                            </div>\n                            <div class=\"term__desc\">\n                                ユーザーは，本サービスの利用にあたり，以下の行為をしてはなりません。\n                            </div>\n                            <ul class=\"term__descExample-items\">\n                                <li class=\"term__descExample-item\">（1）法令または公序良俗に違反する行為</li>\n                                <li class=\"term__descExample-item\">（2）犯罪行為に関連する行為</li>\n                                <li class=\"term__descExample-item\">（3）当社のサーバーまたはネットワークの機能を破壊したり，妨害したりする行為</li>\n                                <li class=\"term__descExample-item\">（4）当社のサービスの運営を妨害するおそれのある行為</li>\n                                <li class=\"term__descExample-item\">（5）他のユーザーに関する個人情報等を収集または蓄積する行為</li>\n                                <li class=\"term__descExample-item\">（6）他のユーザーに成りすます行為</li>\n                                <li class=\"term__descExample-item\">（7）当社のサービスに関連して，反社会的勢力に対して直接または間接に利益を供与する行為</li>\n                                <li class=\"term__descExample-item\">（8）その他，当社が不適切と判断する行為</li>\n                            </ul>\n                        </li> \n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第6条（本サービスの提供の停止等）\n                            </div>\n                            <div class=\"term__desc\">\n                                1.当社は，以下のいずれかの事由があると判断した場合，ユーザーに事前に通知することなく本サービスの全部または一部の提供を停止または中断することができるものとします。\n                            </div>\n                            <ul class=\"term__descExample-items\">\n                                <li class=\"term__descExample-item\">\n                                    （1）本サービスにかかるコンピュータシステムの保守点検または更新を行う場合\n                                </li>\n                                <li class=\"term__descExample-item\">\n                                    （2）地震，落雷，火災，停電または天災などの不可抗力により，本サービスの提供が困難となった場合\n                                </li>\n                                <li class=\"term__descExample-item\">\n                                    （3）コンピュータまたは通信回線等が事故により停止した場合\n                                </li>\n                                <li class=\"term__descExample-item\">\n                                    （4）その他，当社が本サービスの提供が困難と判断した場合\n                                </li>\n                            </ul>\n                            <div class=\"term__desc\">\n                                2.当社は，本サービスの提供の停止または中断により，ユーザーまたは第三者が被ったいかなる不利益または損害について，理由を問わず一切の責任を負わないものとします。\n                            </div>\n                        </li> \n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第7条（利用制限および登録抹消）\n                            </div>\n                            <div class=\"term__desc\">\n                                1.当社は，以下の場合には，事前の通知なく，ユーザーに対して，本サービスの全部もしくは一部の利用を制限し，またはユーザーとしての登録を抹消することができるものとします。\n                            </div>\n                            <ul class=\"term__descExample-items\">\n                                <li class=\"term__descExample-item\">\n                                    （1）本規約のいずれかの条項に違反した場合\n                                </li>\n                                <li class=\"term__descExample-item\">\n                                    （2）登録事項に虚偽の事実があることが判明した場合\n                                </li>\n                                <li class=\"term__descExample-item\">\n                                    （3）その他，当社が本サービスの利用を適当でないと判断した場合\n                                </li>\n                            </ul>\n                            <div class=\"term__desc\">\n                                2.当社は，本条に基づき当社が行った行為によりユーザーに生じた損害について，一切の責任を負いません。\n                            </div>\n                        </li> \n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第8条（免責事項）\n                            </div>\n                            <div class=\"term__desc\">\n                                1.当社の債務不履行責任は，当社の故意または重過失によらない場合には免責されるものとします。\n                            </div>\n                            <div class=\"term__desc\">\n                                2.当社は，何らかの理由によって責任を負う場合にも，通常生じうる損害の範囲内かつ有料サービスにおいては代金額（継続的サービスの場合には1か月分相当額）の範囲内においてのみ賠償の責任を負うものとします。\n                            </div>\n                            <div class=\"term__desc\">\n                                3.当社は，本サービスに関して，ユーザーと他のユーザーまたは第三者との間において生じた取引，連絡または紛争等について一切責任を負いません。\n                            </div>\n                        </li> \n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第9条（サービス内容の変更等）\n                            </div>\n                            <div class=\"term__desc\">\n                                当社は，ユーザーに通知することなく，本サービスの内容を変更しまたは本サービスの提供を中止することができるものとし，これによってユーザーに生じた損害について一切の責任を負いません。\n                            </div>\n                        </li> \n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第10条（利用規約の変更）\n                            </div>\n                            <div class=\"term__desc\">\n                                当社は，必要と判断した場合には，ユーザーに通知することなくいつでも本規約を変更することができるものとします。\n                            </div>\n                        </li> \n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第11条（通知または連絡）\n                            </div>\n                            <div class=\"term__desc\">\n                                ユーザーと当社との間の通知または連絡は，当社の定める方法によって行うものとします。\n                            </div>\n                        </li> \n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第12条（権利義務の譲渡の禁止）\n                            </div>\n                            <div class=\"term__desc\">\n                                ユーザーは，当社の書面による事前の承諾なく，利用契約上の地位または本規約に基づく権利もしくは義務を第三者に譲渡し，または担保に供することはできません。\n                            </div>\n                        </li> \n                        <li class=\"term__item\">\n                            <div class=\"term__ttl\">\n                                第13条（準拠法・裁判管轄）\n                            </div>\n                            <div class=\"term__desc\">\n                                1.本規約の解釈にあたっては，日本法を準拠法とします。\n                            </div>\n                            <div class=\"term__desc\">\n                                2.本サービスに関して紛争が生じた場合には，当社の本店所在地を管轄する裁判所を専属的合意管轄とします。\n                            </div>\n                            \n                        </li> \n                    </ul>\n                    <div class=\"term__btn\">\n                        <div class=\"button_type_square\">\n                            <a ui-sref=\"accountRegister\">\n                                <span>戻る</span>\n                            </a>\n                        </div>\n                    </div>\n                </div>\n";

},{}],50:[function(require,module,exports){
"use strict";

module.exports.tpl = "\n                <div class=\"user\">\n                    <div class=\"user__icon\">\n                        <i>\n                            <img src=\"{{user.iconImg}}?{{user.date}}\"  alt=\"\">\n                        </i>\n                    </div>\n                    <div class=\"user__name\">\n                        {{user.username}}\n                    </div>                    \n                    <div class=\"user__introduce\">\n                        {{user.introduction}}\n                    </div>                              \n                    <div class=\"user__url\">\n                        <a href=\"{{user.hpUrl}}\" target=\"_blank\">{{user.hpUrl}}</a>\n                    </div>\n                </div>\n                <ul class=\"stream__items cf\">\n                    <li class=\"stream__item\" ng-repeat=\"(k,v) in user.projectList\" >\n                        <header class=\"project__head cf\">\n                            <div class=\"project__name\">\n                                {{v.projectName}}\n                            </div>\n                            <div class=\"project__place\">\n                                <span class=\"project__placeIcon\"><img src=\"assets/img/common/icon/placeholder.png\" width=\"13\" height=\"13\" alt=\"\"></span>{{v.projectPlace}}\n                            </div>\n                            <div class=\"project__grade\">\n                                <span>{{v.projectGrade}}</span>\n                            </div>\n                            <div class=\"project__author\">\n                                <span class=\"project__authorIcon\"><img src=\"assets/img/common/icon/avatar.png\" width=\"13\" height=\"13\" alt=\"\"></span>{{v.projectAuthor}}\n                            </div>\n                        </header>\n                        <figure class=\"project__img\">\n                            <img src=\"{{v.projectImg}}\" alt=\"\">\n                        </figure>\n                        <footer class=\"project__foot cf\">\n                            <ul class=\"project-action__items cf\">\n                              <li class=\"project-action__item\">\n                               <div class=\"project__edit\" ng-show=\"(user.loginId == v.projectAuthor)\">\n                                    <a ui-sref=\"projectEdit({projectId:'{{v._id}}'})\">\n                                       <span class=\"project__shareIcon\">\n                                           <img src=\"assets/img/common/icon/edit.png\" width=\"20\" height=\"20\" alt=\"\">\n                                       </span>\n                                    </a>\n                               </div>                                \n                              </li>\n                              <li class=\"project-action__item\">\n                               <div class=\"project__share\">\n                                    <a class=\"btn\" ngclipboard data-clipboard-text=\"https://this-is-open-project.herokuapp.com/project/{{v._id}}\" ng-click=\"user.$commonService.clipboard()\">\n                                       <span class=\"project__shareIcon\">\n                                           <img src=\"assets/img/common/icon/link.png\" width=\"20\" height=\"20\" alt=\"\">\n                                       </span>\n                                    </a>\n                               </div>                           \n                              </li>  \n                            </ul>\n                        </footer>\n                    </li>\n                </ul>\n";

},{}]},{},[1]);
