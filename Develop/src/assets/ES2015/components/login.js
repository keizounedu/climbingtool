const login_tpl = require("../templates/login.js").tpl,
      login_ctrl = require("../controllers/login.js").ctrl
angular.module('App').component('login', {
    controllerAs: 'login',
    template: login_tpl,
    controller: login_ctrl
});
