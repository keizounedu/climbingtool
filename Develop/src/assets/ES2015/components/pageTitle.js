class pageTitle_ctrl {
    constructor($rootScope) {
        const that = this,
            meta = document.createElement('meta');
        this.title = ""
        function pageTitleChange(event, state) {
            that.title = state.title
            document.title = `${state.title} | Open Project`
        }
        $rootScope.$on('$stateChangeSuccess', pageTitleChange);
    }
}
angular.module('App').component('pagetitle', {
    controllerAs: 'pageTitle_ctrl',
    template: `{{pageTitle_ctrl.title}}`,
    controller: pageTitle_ctrl
});
