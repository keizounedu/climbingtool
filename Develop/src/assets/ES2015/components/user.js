const user_tpl = require("../templates/user.js").tpl,
      user_ctrl = require("../controllers/user.js").ctrl
angular.module('App').component('user', {
    controllerAs: 'user',
    template: user_tpl,
    controller: user_ctrl
});
