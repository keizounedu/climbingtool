const pushNotification_tpl = require("../templates/pushNotification.js").tpl,
      pushNotification_ctrl = require("../controllers/pushNotification.js").ctrl
angular.module('App').component('pushnotification', {
    controllerAs: 'pushNotification',
    template: pushNotification_tpl,
    controller: pushNotification_ctrl
});
