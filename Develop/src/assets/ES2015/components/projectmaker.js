const projectmaker_tpl = require("../templates/projectMaker.js").tpl,
      projectmaker_ctrl = require("../controllers/projectMaker.js").ctrl
angular.module('App').component('projectmaker', {
    controllerAs: 'projectmaker',
    template: projectmaker_tpl,
    controller: projectmaker_ctrl
});
