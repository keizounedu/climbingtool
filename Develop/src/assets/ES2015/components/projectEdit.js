const projectEdit_tpl = require("../templates/projectEdit.js").tpl,
      projectEdit_ctrl = require("../controllers/projectEdit.js").ctrl
angular.module('App').component('projectedit', {
    controllerAs: 'projectEdit',
    template: projectEdit_tpl,
    controller: projectEdit_ctrl
});
