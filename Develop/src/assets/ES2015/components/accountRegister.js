const accountRegister_tpl = require("../templates/accountRegister.js").tpl,
      accountRegister_ctrl = require("../controllers/accountRegister.js").ctrl
angular.module('App').component('accountregister', {
    controllerAs: 'accountRegister',
    template: accountRegister_tpl,
    controller: accountRegister_ctrl
});
