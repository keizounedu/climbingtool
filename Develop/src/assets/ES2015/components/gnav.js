const gnav_tpl = require("../templates/gnav.js").tpl,
      gnav_ctrl = require("../controllers/gnav.js").ctrl
angular.module('App').component('gnav', {
    controllerAs: 'gnav',
    template: gnav_tpl,
    controller: gnav_ctrl
});
