const term_tpl = require("../templates/term.js").tpl,
      term_ctrl = require("../controllers/term.js").ctrl
angular.module('App').component('term', {
    controllerAs: 'term',
    template: term_tpl,
    controller: term_ctrl
});
