const stream_tpl = require("../templates/stream.js").tpl,
      stream_ctrl = require("../controllers/stream.js").ctrl
angular.module('App').component('stream', {
    controllerAs: 'stream',
    template: stream_tpl,
    controller: stream_ctrl
});
