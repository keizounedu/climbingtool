const accountEdit_tpl = require("../templates/accountEdit.js").tpl,
      accountEdit_ctrl = require("../controllers/accountEdit.js").ctrl
angular.module('App').component('accountedit', {
    controllerAs: 'accountEdit',
    template: accountEdit_tpl,
    controller: accountEdit_ctrl
});
