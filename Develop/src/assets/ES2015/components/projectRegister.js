const projectRegister_tpl = require("../templates/projectRegister.js").tpl,
      projectRegister_ctrl = require("../controllers/projectRegister.js").ctrl
angular.module('App').component('projectregister',{
  controllerAs: 'projectRegister',
  template: projectRegister_tpl,
  controller: projectRegister_ctrl
});