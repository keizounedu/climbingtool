    angular.module('App').config(function($locationProvider,$stateProvider, $urlRouterProvider) {
        $locationProvider.html5Mode(true).hashPrefix('*');
        $urlRouterProvider.otherwise('/login');
        $stateProvider
            .state('login', {
                url: "/login",
                views:{
                    "mainContens":{template: "<login></login>"},
                    "glovalNavigation":{template: ``}
                },
                title: 'Login'
            })
            .state('term', {
                url: "/term",
                views:{
                    "mainContens":{template: "<term></term>"},
                    "glovalNavigation":{template: ``}
                },
                title: 'Term'
            })            
            .state('accountRegister', {
                url: "/accountRegister",
                views:{
                    "mainContens":{template: "<accountregister>"},
                    "glovalNavigation":{template: ``}
                },
                title: 'Account Register'
            })
            .state('accountEdit', {
                url: "/accountEdit",
                views:{
                    "mainContens":{template: "<accountedit>"},
                    "glovalNavigation":{template: "<gnav>"}
                },
                title: 'Account Edit'
            })
            .state('projectRegister', {
                url: "/projectRegister",
                views:{
                    "mainContens":{template: "<projectregister>"},
                    "glovalNavigation":{template: "<gnav>"}
                },
                title: 'Project Register'
            })
            .state('userInfo', {
                url: "/user/:userId",
                views:{
                    "mainContens":{template: "<user>"},
                    "glovalNavigation":{template: "<gnav>"}
                },
                title: 'User Information'
            })
            .state('projectEdit', {
                url: "/projectEdit/:projectId",
                views:{
                    "mainContens":{template: "<projectedit>"},
                    "glovalNavigation":{template: "<gnav>"}
                },
                title: 'Project Edit'
            })            
            .state('stream', {
                url: "/stream",
                views:{
                    "mainContens":{template: "<stream>"},
                    "glovalNavigation":{template: "<gnav>"}
                },
                title: 'Stream'
            })    
            .state('projectMaker', {
                url: "/projectMaker",
                views:{
                    "mainContens":{template: "<projectmaker>"},
                    "glovalNavigation":{template: "<gnav>"}
                },
                template: "<create>",
                title: 'Project Maker'
            })
    });
