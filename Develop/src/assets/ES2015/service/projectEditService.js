(function() {
    'use strict';
    class projectEditService {
        constructor(commonService, messageService) {
            this.$messageService = messageService
            this.$commonService = commonService
        }
        getProjectInfo(request) {
            let d = this.$commonService.$q.defer(),
                data = this.$commonService.$http.getProjectInfo(request);
            data.success((response) => {
                d.resolve(response);
            }).error(() => {
                d.resolve([]);
            })
            return d.promise;
        }
        edit(fd) {
            let d = this.$commonService.$q.defer(),
                data = this.$commonService.$http.editProject(fd);
            data.success(() => {
                d.resolve(true);
            }).error(() => {
                d.resolve(false);

            })
            return d.promise;
        }
        delete(request) {
            let d = this.$commonService.$q.defer(),
                data = this.$commonService.$http.deleteProject(request);
            data.success(() => {
                d.resolve(true);
            }).error((response) => {
                d.resolve(false);
            })
            return d.promise;
        }
    }
    angular.module('App').service('projectEditService', (commonService, messageService) => new projectEditService(commonService, messageService));
})()
