(function() {

    'use strict';
    class commonService {
        constructor($cookies, $state, $q, $timeout, http, cookieService, messageService, pushNotificationService) {
            this.cashElement($cookies, $state, $q, $timeout, http, cookieService, messageService, pushNotificationService)
        }
        cashElement($cookies, $state, $q, $timeout, http, cookieService, messageService, pushNotificationService) {

            this.$cookies = $cookies
            this.$state = $state
            this.$q = $q
            this.$timeout = $timeout
            this.$http = http

            this.$cookieService = cookieService
            this.$messageService = messageService
            this.$pushNotificationService = pushNotificationService

            this.loader__wrap = document.querySelector('.loader__wrap')

            this.projectGrade = ["V0", "V1", "V2", "V3", "V4", "V5", "V6", "V7", "V8", "V9", "V10", "V11", "V12", "V13", "V14", "V15", "V16"];

        }
        spaceReplace(t) {
            let temp = t.trim()
            return temp
        }
        logout() {
            this.$cookies.remove("loginId")
            this.$cookies.remove("loginState")
            this.$cookies.remove("editProject")
            this.$state.go('login')
        }
        clipboard() {
            this.$pushNotificationService.output(this.$messageService.clipboard_msg_200())
        }
        loader_visible(){
            this.loader__wrap.style.display = "block"
        }
        loader_inVisible(){
            this.loader__wrap.style.display = "none"
        }
    }
    angular.module('App').factory('commonService', ($cookies, $state, $q, $timeout, http, cookieService, messageService, pushNotificationService) => new commonService($cookies, $state, $q, $timeout, http, cookieService, messageService, pushNotificationService));
})()
