(function() {
    'use strict';
    class accountRegisterService {
        constructor(commonService, messageService) {
            this.message = ""
            this.$messageService = messageService
            this.$commonService = commonService
        }
        register(fd) {
            let d = this.$commonService.$q.defer(),
                data = this.$commonService.$http.putUserInfo(fd);
            data.success(() => {
                d.resolve("complete");
            }).error(() => {
                d.resolve("fail");
            })
            return d.promise;
        }

    }
    angular.module('App').service('accountRegisterService', (commonService, messageService) => new accountRegisterService(commonService, messageService));
})()
