(function() {
    'use strict';
    class http {
        constructor($http, $httpParamSerializerJQLike) {
            this.$http = $http;
            this.$httpParamSerializerJQLike = $httpParamSerializerJQLike;
        }
        e(option) {
            return this.$http(option)
        }
        auth(username, password) {
            return this.$http({
                method: 'POST',
                url: '/api/user/login/',
                data: {
                    "username": username,
                    "password": password
                }
            })
        }
        editUserInfo(fd) {
            return this.$http({
                method: 'POST',
                url: '/api/user/edit/',
                transformRequest: null,
                headers: {
                    'Content-Type': undefined
                },
                data: fd
            })
        }
        deleteUserInfo(request) {
            return this.$http({
                method: 'DELETE',
                url: '/api/user/delete/' + request._id
            })
        }
        getUserInfo(username) {
            return this.$http({
                method: 'POST',
                url: '/api/user/info/',
                data: {
                    "username": username,
                }
            })
        }
        putUserInfo(fd) {
            return this.$http({
                method: 'PUT',
                url: '/api/user/create/',
                transformRequest: null,
                headers: {
                    'Content-Type': undefined
                },
                data: fd
            })
        }
        getProjectInfo(request) {
            return this.$http({
                method: 'POST',
                url: '/api/project/',
                data: request
            })
        }
        editProject(fd) {
            return this.$http({
                method: 'POST',
                url: '/api/project/edit/',
                transformRequest: null,
                headers: {
                    'Content-Type': undefined
                },
                data: fd
            })
        }
        deleteProject(request) {
            return this.$http({
                method: 'DELETE',
                url: '/api/project/delete/' + request._id
            })
        }
        putProject(fd) {
            return this.$http({
                method: 'PUT',
                url: '/api/project/create/',
                transformRequest: null,
                headers: {
                    'Content-Type': undefined
                },
                data: fd
            })
        }
    }
    angular.module('App').factory('http', ($http, $httpParamSerializerJQLike) => new http($http, $httpParamSerializerJQLike));
})()
