(function() {
  'use strict';
  class gapiService{
    constructor(){
      this.cashElement()
    }
    cashElement(){
        // The Browser API key obtained from the Google Developers Console.
        this.developerKey = 'AIzaSyCFKWcEzDknxRWIaxJdKsihvfdF8Mvv8T0'
        // The Client ID obtained from the Google Developers Console. Replace with your own Client ID.
        this.clientId = "677936638338-g0d0k9q6g6kt3djgj3e9m9ac313t1del.apps.googleusercontent.com";
        this.authApiLoaded = false;
        this.pickerApiLoaded = false;
        this.oauthToken="";
        this.viewIdForhandleAuthResult="";
        this.setOAuthToken = true;
        this.pickerObj={};
        this.pickerRes = {};
        this.onApiLoad()
      }
      onApiLoad(){
        gapi.load('auth', {'callback': this.onAuthApiLoad(this)});
        gapi.load('picker', {'callback': this.onPickerApiLoad(this)});
      }
      onAuthApiLoad(that){
        that.authApiLoaded = true;
      }
      onPickerApiLoad(that){
        that.pickerApiLoaded = true;
      }
      handleAuthResult(authResult){
        if (authResult && !authResult.error) {
          this.oauthToken = authResult.access_token;
          this.createPicker(this.viewIdForhandleAuthResult, true);
        }
      }
      createPicker(that,viewId,setOAuthToken) {
        console.log(this)
        if (this.authApiLoaded && this.pickerApiLoaded) {
          var picker;

          if(this.authApiLoaded && this.oauthToken && this.setOAuthToken) {
            that.pickerObj = new google.picker.PickerBuilder().
            addView(viewId).
            setLocale('ja').
            setOAuthToken(this.oauthToken).
            setDeveloperKey(this.developerKey).
            setCallback(pickerCallback).
            build();
          } else {
            that.pickerObj = new google.picker.PickerBuilder().
            addView(viewId).
            setLocale('ja').
            setDeveloperKey(this.developerKey).
            setCallback(pickerCallback).
            build();
          }
          that.pickerObj.setVisible(true);
        }

        function pickerCallback(data) {
          if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
            window.openProject.pickerRes = ""
            window.openProject.pickerRes = JSON.stringify(data[google.picker.Response.DOCUMENTS][0], null, "  ");
          }
        }

      }
      picker(){
        this.setOAuthToken = false;        
        this.createPicker(this,google.picker.ViewId.MAPS,this.setOAuthToken);
      }
    }
    angular.module('App').service('gapiService', () => new gapiService());
  })()