(function() {
    'use strict';
    class imageGenarator {
        /* @ngInject */
        constructor() {}
        cashElements() {
            this.item_count = 0;
            this.b = document.body
            this.items = new Array()
            this.fileInput = document.getElementById('fileInput')
            this.overlay = document.querySelector('.overlay')
            this.create__editorCtrl = document.querySelector('.create__editorCtrl')
            this.create__imageEditor = document.querySelector('.create__imageEditor')
            this.create__registerBtn = document.querySelector('.create__registerBtn')
            this.overlay__resultImg = document.querySelector('.overlay__resultImg')
            this.overlay_close = document.querySelectorAll('.overlay__close')
            this.loader = document.querySelector('.loader__wrap')
        }
        bindEvents() {
            let that = this;
            this.fileInput.addEventListener('change', function(e) {
                that.base_image_set(e)
            });
            this.create__registerBtn.addEventListener('click', function(e) {
                that.genarateProject()
            });
            for (var i = 0; i < this.overlay_close.length; i++) {
                this.overlay_close[i].addEventListener('click', function(e) {
                    that.element_reset()
                });
            }
        }
        base_image_set(e) {
            let that = this,
                img = e.target.files[0],
                orientation_img = "",
                compress_img = "";
            // this.loader.style.height = window.height
            this.loader.style.display = "block"
            EXIF.getData(img, function() {
                orientation_img = img.exifdata.Orientation
                compress_img = new MegaPixImage(img)
                that.create__imageEditor.innerHTML = '<p class="create__imageBase"><img src="" alt="" id="base_img"></p>';
                compress_img.render(document.getElementById('base_img'), { orientation: orientation_img });
                document.getElementById('base_img').addEventListener('load', function(e) {
                    that.create__editorCtrl.style.display = "block"
                    that.create__registerBtn.style.display = "table"
                    that.loader.style.display = "none"

                });
            });
        }
        element_reset() {
            let deleteTarget = document.querySelectorAll('.create__iconImg')
            for (let i = 0; i < deleteTarget.length; i++) {
                deleteTarget[i].remove()
            }
            this.overlay.style.display = "none"
        }
        genarateProject() {
            let that = this,
                cnv = document.getElementById('phantom_cnv'),
                ctx = cnv.getContext("2d"),
                baseImage = document.getElementById('base_img'),
                imageElement = new Image(baseImage.offsetWidth * 0.5, baseImage.offsetHeight * 0.5),
                iconImages = document.querySelectorAll('.create__iconImg'),
                iconImagesArr = new Array(),
                loadcount = 1;

            // this.loader.style.height = window.height
            this.loader.style.display = "block"

            for (let i = 0; i < iconImages.length; i++) {
                iconImagesArr.push(iconImages[i])
            }

            cnv.width = baseImage.offsetWidth
            cnv.height = baseImage.offsetHeight
            ctx.fillRect(0, 0, baseImage.offsetWidth, baseImage.offsetHeight)

            imageElement.src = baseImage.getAttribute('src')
            imageElement.onload = function() {
                ctx.drawImage(imageElement, 0, 0, baseImage.offsetWidth, baseImage.offsetHeight);
                if (iconImages.length == 0) {
                    overlayDisplay()
                } else {
                    iconImagesArr.forEach(function(val, index, ar) {
                        var tmp_parts_img = new Image(val.getAttribute('data-w'), val.getAttribute('data-h'))
                        tmp_parts_img.src = val.getAttribute('data-src') + "?" + new Date().getTime()
                        elementImgCanvasSet(tmp_parts_img, val)
                    });
                }
            }

            function elementImgCanvasSet(t, v) {
                t.onload = function(e) {
                    loadcount++
                    ctx.drawImage(t, v.getAttribute('data-x'), v.getAttribute('data-y'), v.getAttribute('data-w'), v.getAttribute('data-h'));
                    if (iconImages.length <= loadcount) {
                        overlayDisplay()
                    }
                }
            }

            function overlayDisplay() {
                console.log(that.b)
                Velocity(that.b, 'scroll', {
                    duration: 450,
                    easing: 'easeInOutCubic',
                    complete: function() {
                        that.loader.style.display = "none"
                        that.overlay__resultImg.style.width = baseImage.offsetWidth
                        that.overlay__resultImg.style.height = baseImage.offsetHeight
                        that.overlay__resultImg.setAttribute('src', cnv.toDataURL("image/png"))
                        that.overlay__resultImg.style.display = "block"
                        that.overlay.style.display = "block"
                    }
                })
            }

        }
    }
    angular.module('App').factory('imageGenarator', () => new imageGenarator());
})()
