(function() {
    'use strict';
    class loginService {
        constructor(commonService, messageService) {
            this.$commonService = commonService
            this.$messageService = messageService
            this.message = ""
        }
        login(username, password) {
            let d = this.$commonService.$q.defer(),
                data = this.$commonService.$http.auth(username, password);
            data.success((response) => {
                if (response.username == username && response.password == password) {
                    this.$commonService.$cookieService.$cookies.put("loginState", true, ["expires", 7])
                    this.$commonService.$cookieService.$cookies.put("loginId", username, ["expires", 7])
                    this.$commonService.$state.go("userInfo", { "userId": username })
                } else {
                    this.message = this.$messageService.login_msg_400()
                }
                d.resolve(this.message);
            }).error((response) => {
                this.message = this.$messageService.login_msg_400()
                d.resolve(this.message);
            })
            return d.promise;
        }

    }
    angular.module('App').service('loginService', (commonService, messageService) => new loginService(commonService, messageService));
})()
