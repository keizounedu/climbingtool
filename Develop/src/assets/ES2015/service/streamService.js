(function() {
    'use strict';
    class streamService {
        constructor(commonService, messageService) {
            this.$messageService = messageService
            this.$commonService = commonService
        }
        getProjectInfo() {
            const d = this.$commonService.$q.defer(),
                data = this.$commonService.$http.getProjectInfo();
            data.success((response) => {
                d.resolve(response);
            }).error((response) => {
                d.resolve(response);
            })
            return d.promise;
        }
    }
    angular.module('App').service('streamService', (commonService, messageService) => new streamService(commonService, messageService));
})()
