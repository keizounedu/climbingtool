(function() {
    'use strict';
    class messageService {
        constructor() {}
        function_interruption_msg(){
            return "処理を中断しました。"            
        }
        clipboard_msg_200(){
            return "クリップボードにURLをコピーしました。"
        }
        login_msg_200() {
            return "ログインが完了しました。"
        }
        login_msg_400() {
            return "入力されたユーザーネームはアカウントと一致しません。ユーザーネームをご確認の上、もう一度実行してください。"
        }
        login_msg_503() {
            return "入力されたユーザーネームはアカウントと一致しません。ユーザーネームをご確認の上、もう一度実行してください。"
        }
        projectRegister_msg_200(){
            return "プロジェクトの作成が完了しました。"            
        }
        projectRegister_msg_400(){
            return "プロジェクトの作成に失敗しました。"            
        }        
        projectDelete_msg_200(){
            return "プロジェクトの削除が完了しました。"
        }
        projectDelete_msg_400(){
            return "プロジェクトの削除に失敗しました。"
        }        
        projectEdit_msg_200(){
            return "プロジェクトの修正が完了しました。"
        }
        projectEdit_msg_400(){
            return "プロジェクトの修正に失敗しました。"
        }        
        accoutResiger_msg_200(){
            return "アカウントの作成が完了しました。"
        }
        accoutResiger_msg_400() {
            return "入力されたユーザーネーム、メールアドレスはすでに使用されています。"
        }
        accoutEdit_msg_200(){
            return "アカウントの修正が完了しました。"
        }
        accoutEdit_msg_400(){
            return "アカウントの修正に失敗しました。"
        }        
        accountDelete_msg_200(){
            return "アカウントの削除が完了しました。"
        }
        accountDelete_msg_400(){
            return "アカウントの削除に失敗しました。"
        }        

    }
    angular.module('App').service('messageService', () => new messageService());
})()
