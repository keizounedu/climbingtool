(function() {
    'use strict';
    class projectRegisterService {
        constructor(commonService, messageService) {
            this.$messageService = messageService
            this.$commonService = commonService
        }
        register(fd) {
            const d = this.$commonService.$q.defer(),
                data = this.$commonService.$http.putProject(fd);
            data.success((response) => {
                d.resolve(true);
            }).error((response) => {
                d.resolve(false);
            })
            return d.promise;
        }
    }
    angular.module('App').service('projectRegisterService', (commonService, messageService) => new projectRegisterService(commonService, messageService));
})()
