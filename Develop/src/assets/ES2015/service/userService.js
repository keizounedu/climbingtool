(function() {
    'use strict';
    class userService {
        constructor(commonService, messageService) {
            this.$messageService = messageService
            this.$commonService = commonService
        }
        getUserInfo(username) {
            const d = this.$commonService.$q.defer(),
                data = this.$commonService.$http.getUserInfo(username);
            data.success((response) => {
                d.resolve(response);
            }).error((response) => {
                d.resolve(response);
            })
            return d.promise;
        }
        getProjectInfo(request) {
            const d = this.$commonService.$q.defer(),
                data = this.$commonService.$http.getProjectInfo(request);
            data.success((response) => {
                d.resolve(response);
            }).error(() => {
                d.resolve([]);
            })
            return d.promise;
        }
    }
    angular.module('App').service('userService', (commonService, messageService) => new userService(commonService, messageService));
})()
