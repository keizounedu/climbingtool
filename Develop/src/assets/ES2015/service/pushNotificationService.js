(function() {
    'use strict';
    class pushNotificationService {
        constructor($timeout) {
            this.cashElement($timeout)
        }
        cashElement($timeout) {
            this.$timeout = $timeout
            this.message = "";
            this.display = false;
        }
        output(msg) {
            this.message = msg;
            this.display = true;
            this.$timeout(() => {
                this.display = false;
            }, 5000)
        }
    }
    angular.module('App').factory('pushNotificationService', ($timeout) => new pushNotificationService($timeout));
})()
