(function() {
    'use strict';
    class accountEditService {
        constructor(commonService, messageService) {
            this.$commonService = commonService
            this.$messageService = messageService
        }
        getUserInfo(username) {
            let d = this.$commonService.$q.defer(),
                data = this.$commonService.$http.getUserInfo(username);
            data.success((response) => {
                d.resolve(response);
            }).error((response) => {
                d.resolve(response);
            })
            return d.promise;
        }
        edit(fd) {
            let d = this.$commonService.$q.defer(),
                data = this.$commonService.$http.editUserInfo(fd);
            data.success(() => {
                d.resolve(true);
            }).error(() => {
                d.resolve(false);
            })
            return d.promise;
        }
        delete(request) {
            let d = this.$commonService.$q.defer(),
                data = this.$commonService.$http.deleteUserInfo(request);
            data.success(() => {
                d.resolve(true);
            }).error(() => {
                d.resolve(false);
            })
            return d.promise;
        }
    }
    angular.module('App').service('accountEditService', (commonService, messageService) => new accountEditService(commonService, messageService));
})()
