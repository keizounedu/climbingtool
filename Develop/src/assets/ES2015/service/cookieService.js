(function() {
    'use strict';
    class cookieService{
      constructor($cookies,$state){
        this.cashElement($cookies,$state)
        this.bindEvents()        
      }
      cashElement($cookies,$state){
        this.$cookies = $cookies
        this.$state = $state
      }
      bindEvents(){
        window.addEventListener("hashchange", () => {
          this.haveCookieCheck()
        }, false);
      }
      haveCookieCheck(){
        if(!this.$cookies.get('loginState') && this.$state.current.name != "accountRegister" && this.$state.current.name != "term"){
          this.$state.go('login')              
        } else {
          if(this.$state.current.name == "login"){
            this.$state.go("userInfo",{"userId":this.$cookies.get("loginId")})
          }
        }
      }
    }
    angular.module('App').service('cookieService', ($cookies,$state) => new cookieService($cookies,$state));
})()