(function(root) {

    class createElements {
        /* @ngInject */
        constructor() {}
        cacheElements() {
            this.add_btn = document.querySelectorAll('.element-add')
            this.create__imageEditor = document.querySelector('.create__imageEditor')
            this.current_element = 0
        }
        bindEvents() {
            var that = this
            for (var i = 0; i < this.add_btn.length; i++) {
                that.add_btn[i].addEventListener('click', function(e) {
                    that.element_add(this)
                });
            }
        }
        element_delete(target) {
            target.remove()
        }
        element_add(target) {
            var that = this,
                setElement = "",
                markType = target.getAttribute('data-marktype'),
                colorType = target.getAttribute('data-colortype'),                
                setNum = that.current_element,
                setElementName = "create__" + markType + "Img" + setNum,
                itemH = markType.match(/circle/) == null ? "35px" : "112px"
            setElement = document.createElement("div");
            setElement.setAttribute('id', setElementName)
            setElement.setAttribute('class', "create__iconImg create__iconItem--" + target.getAttribute('data-type'))
            setElement.setAttribute('data-src', "../assets/img/create/"+ colorType +"/" + markType + ".png")
            setElement.setAttribute('data-x', "0");
            setElement.setAttribute('data-y', "0");
            setElement.setAttribute('data-w', "120");
            setElement.setAttribute('data-h', itemH);
            setElement.style.width = "120px"
            setElement.style.height = itemH
            setElement.style.top = "0px"
            setElement.style.left = "0px"
            setElement.innerHTML = `
                <div id="create__${markType}ImgInner${setNum}" class="create__iconImgInner">
                <p class="create__imageBase">
                    <img src="../assets/img/create/${colorType}/${markType}.png" alt="" >
                </p>
                </div>
             `;
            that.create__imageEditor.appendChild(setElement);
            this.element_bind_event(setElementName)
            that.current_element++;
        }
        element_bind_event(TARGET) {

            var that = this,
                D_TAP_EVENT = "";

            D_TAP_EVENT = new Hammer(document.getElementById(TARGET));
            D_TAP_EVENT.on('doubletap', function(ev) {
                that.element_delete(document.getElementById(TARGET))
            });

            interact('#' + TARGET)
                .draggable({
                    onmove: that.dragMoveListener
                })
                .resizable({
                    preserveAspectRatio: true,
                    edges: { left: true, right: true, bottom: true, top: true }
                })
                .on('resizemove', that.resizemove);

        }
        resizemove(event) {
            var target = event.target,
                x = (parseFloat(target.getAttribute('data-x')) || 0),
                y = (parseFloat(target.getAttribute('data-y')) || 0),
                w = event.rect.width,
                h = event.rect.height

            target.style.width = event.rect.width + 'px';
            target.style.height = event.rect.height + 'px';

            x += event.deltaRect.left;
            y += event.deltaRect.top;

            target.style.left = x + 'px'
            target.style.top = y + 'px'

            // target.style.webkitTransform = target.style.transform =
            //     'translate(' + x + 'px,' + y + 'px)';

            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
            target.setAttribute('data-w', w);
            target.setAttribute('data-h', h);
            // target.textContent = Math.round(event.rect.width) + '×' + Math.round(event.rect.height);

        }
        dragMoveListener(event) {
            var target = event.target,
                x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy,
                w = event.target.style.width.split('px')[0],
                h = event.target.style.height.split('px')[0]

            // target.style.webkitTransform =
            //     target.style.transform =
            //     'translate(' + x + 'px, ' + y + 'px)';

            target.style.left = x + 'px'
            target.style.top = y + 'px'

            // target.style.webkitTransform =
            //     target.style.transform =
            //     'translate(' + x + 'px, ' + y + 'px)';

            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
            target.setAttribute('data-w', w);
            target.setAttribute('data-h', h);
        }

    }

    angular.module('App').factory('createElements', () => new createElements());

}(window))
