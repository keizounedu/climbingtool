module.exports.tpl = `
                <div class="user">
                    <div class="user__icon">
                        <i>
                            <img src="{{user.iconImg}}?{{user.date}}"  alt="">
                        </i>
                    </div>
                    <div class="user__name">
                        {{user.username}}
                    </div>                    
                    <div class="user__introduce">
                        {{user.introduction}}
                    </div>                              
                    <div class="user__url">
                        <a href="{{user.hpUrl}}" target="_blank">{{user.hpUrl}}</a>
                    </div>
                </div>
                <ul class="stream__items cf">
                    <li class="stream__item" ng-repeat="(k,v) in user.projectList" >
                        <header class="project__head cf">
                            <div class="project__name">
                                {{v.projectName}}
                            </div>
                            <div class="project__place">
                                <span class="project__placeIcon"><img src="assets/img/common/icon/placeholder.png" width="13" height="13" alt=""></span>{{v.projectPlace}}
                            </div>
                            <div class="project__grade">
                                <span>{{v.projectGrade}}</span>
                            </div>
                            <div class="project__author">
                                <span class="project__authorIcon"><img src="assets/img/common/icon/avatar.png" width="13" height="13" alt=""></span>{{v.projectAuthor}}
                            </div>
                        </header>
                        <figure class="project__img">
                            <img src="{{v.projectImg}}" alt="">
                        </figure>
                        <footer class="project__foot cf">
                            <ul class="project-action__items cf">
                              <li class="project-action__item">
                               <div class="project__edit" ng-show="(user.loginId == v.projectAuthor)">
                                    <a ui-sref="projectEdit({projectId:'{{v._id}}'})">
                                       <span class="project__shareIcon">
                                           <img src="assets/img/common/icon/edit.png" width="20" height="20" alt="">
                                       </span>
                                    </a>
                               </div>                                
                              </li>
                              <li class="project-action__item">
                               <div class="project__share">
                                    <a class="btn" ngclipboard data-clipboard-text="https://this-is-open-project.herokuapp.com/project/{{v._id}}" ng-click="user.$commonService.clipboard()">
                                       <span class="project__shareIcon">
                                           <img src="assets/img/common/icon/link.png" width="20" height="20" alt="">
                                       </span>
                                    </a>
                               </div>                           
                              </li>  
                            </ul>
                        </footer>
                    </li>
                </ul>
`