module.exports.tpl = `
                <div class="accountEdit">
                    <form name="accountEditForm" class="accountEdit-form" id="accountEdit-form" enctype="multipart/form-data">
                        <div class="accountEdit-form__item">
                            <span>プロフィール写真：</span>
                            <span><img src="{{accountEdit.iconImg}}" alt="" width="200px" height="auto"></span>
                            <input type="file" name="file[]" id="file">
                        </div>
                        <div class="accountEdit-form__item">
                            <span>パスワード：</span>
                            <input type="password" name="password" placeholder="password" ng-required="true" ng-model="accountEdit.password">
                        </div>                        
                        <div ng-show="accountEditForm.$submitted || accountEditForm.password.$touched">
                            <p class="errorMessage" ng-show="accountEditForm.password.$error.required">パスワードは必須です。</p>
                        </div>
                        <div class="accountEdit-form__item">
                            <span>メールアドレス：</span>
                            <input type="email" name="mailAddress" placeholder="hoge@hoge.com" ng-required="true" ng-model="accountEdit.mailAddress">
                        </div>
                        <div ng-show="accountEditForm.$submitted || accountEditForm.mailAddress.$touched">
                            <p class="errorMessage" ng-show="accountEditForm.mailAddress.$error.required">メールアドレスは必須です。</p>
                            <p class="errorMessage" ng-show="accountEditForm.mailAddress.$error.email">メールアドレスを入力してください。</p>                            
                        </div>                        
                        <div class="accountEdit-form__item">
                            <span>ウェブサイト：</span>
                            <input type="text" name="website" placeholder="http://hogehoge.com/" ng-model="accountEdit.hpUrl">
                        </div>
                        <div class="accountEdit-form__item">                            
                            <span>自己紹介：</span>
                            <textarea name="selfIntroduce" id="" cols="30" rows="10" placeholder="HogeHoge" ng-model="accountEdit.introduction"></textarea>
                        </div>
                        <div class="accountEdit__message">
                            {{accountEdit.message}}
                        </div>                
                        <div class="accountEdit-form__item ranks_type_parallel__items">   
                            <div class="ranks_type_parallel__item button_type_circle" ng-click="accountEdit.edit()">
                                <a >
                                    <span>編集</span>
                                </a>
                            </div>
                            <div class="ranks_type_parallel__item button_type_circle" ng-click="accountEdit.delete()">
                                <a>
                                    <span>削除</span>
                                </a>
                            </div>      
                        </div> 
                    </form>
                </div>
`