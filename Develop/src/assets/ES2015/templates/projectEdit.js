module.exports.tpl = `
                <div class="projectEdit">
                    <div class="projectEdit-preview">
                        <img src="{{projectEdit.projectImg}}" alt="">
                    </div>
                    <form name="projectEditForm" id="projectEdit-form" class="projectEdit-form" enctype="multipart/form-data">
                        <input type="hidden" ng-model="projectEdit.projectId" value="{{projectEdit.projectId}}" name="projectId">
                        <div class="projectEdit-form__item">
                            <span>課題の名前：</span>
                            <input type="text" name="projectName" ng-model="projectEdit.projectName">
                        </div>
                        <div class="projectEdit-form__item">
                            <span>課題のある場所：</span>
                            <input type="text" name="projectPlace" ng-model="projectEdit.projectPlace">
                        </div>
                        <div class="projectEdit-form__item">
                            <span>グレード：</span>
                            <select name="projectGrade" ng-required="true" ng-model="projectEdit.projectGrade" ng-options="grade for grade in projectEdit.projectGradeList">
                                <option value="">グレードを選択してください</option>
                            </select>
                        </div>
                        <div class="projectRegister-form__item form__item item_type_radio">
                            <span>プライバシー設定：</span>
                            公開：<input type="radio" ng-model="projectEdit.isPublic" value="true" name="isPublic">&nbsp;
                            自分のみ：<input type="radio" ng-model="projectEdit.isPublic" value="false" name="isPublic">
                        </div>
                        <div class="errorMessage">
                            {{projectEdit.message}}
                        </div>
                        <div class="projectEdit-form__item ranks_type_parallel__items">   
                            <div class="projectEdit-form__submit ranks_type_parallel__item button_type_circle" ng-click="projectEdit.edit()">
                                <a>
                                    <span>編集</span>
                                </a>
                            </div>
                            <div class="projectEdit-form__submit ranks_type_parallel__item button_type_circle" ng-click="projectEdit.delete()">
                                <a>
                                    <span>削除</span>
                                </a>
                            </div>      
                        </div>              
                    </form>
                </div>
`

                        // <div class="projectEdit-form__item">
                        //     <span>課題の写真：</span>
                        //     <input type="file" name="file[]" id="file">
                        // </div>