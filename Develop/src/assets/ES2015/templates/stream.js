module.exports.tpl = `
                <div class="stream__search">
                    <form action="" class="stream__form">
                        <div class="project__searchInput">
                            <input type="text" placeholder="検索" ng-model="searchText">
                        </div>
                    </form>
                </div>
                <ul class="stream__items">
                    <li class="stream__item" ng-repeat="(k,v) in stream.projectList | filter:searchText" ng-show="v.isPublic || stream.username != v.projectAuthor">
                        <header class="project__head cf">
                            <div class="project__name">
                                {{v.projectName}}
                            </div>
                            <div class="project__place">
                                <span class="project__placeIcon"><img src="assets/img/common/icon/placeholder.png" width="13" height="13" alt=""></span>{{v.projectPlace}}
                            </div>
                            <div class="project__grade">
                                <span>{{v.projectGrade}}</span>
                            </div>
                            <div class="project__author">
                                <span class="project__authorIcon"><img src="assets/img/common/icon/avatar.png" width="13" height="13" alt=""></span><a ui-sref="userInfo({userId:'{{v.projectAuthor}}'})">{{v.projectAuthor}}</a>
                            </div>
                        </header>
                        <figure class="project__img">
                            <img src="{{v.projectImg}}" alt="">
                        </figure>
                        <footer class="project__foot cf">
                            <ul class="project-action__items cf">
                              <li class="project-action__item">
                               <div class="project__edit" ng-show="(stream.username == v.projectAuthor)">
                                    <a ui-sref="projectEdit({projectId:'{{v._id}}'})" >
                                       <span class="project__shareIcon">
                                           <img src="assets/img/common/icon/edit.png" width="20" height="20" alt="">
                                       </span>
                                    </a>
                               </div>                                
                              </li>
                              <li class="project-action__item">
                               <div class="project__share">
                                    <a class="btn" ngclipboard data-clipboard-text="https://this-is-open-project.herokuapp.com/project/{{v._id}}" ng-click="stream.$commonService.clipboard()">
                                       <span class="project__shareIcon">
                                           <img src="assets/img/common/icon/link.png" width="20" height="20" alt="">
                                       </span>
                                    </a>
                               </div>                           
                              </li>  
                            </ul>
                        </footer>
                    </li>
                </ul>
`