module.exports.tpl = `
            <div class="accountRegister">                     
                <form ng-submit="accountRegisterForm.$valid && accountRegister.register()" novalidate id="accountRegister-form" class="accountRegister-form form" name="accountRegisterForm">
                    <div class="accountRegister-form__item form__item">
                        <input type="email" name="mailaddress" ng-required="true" placeholder="メールアドレス" ng-model="accountRegister.mailaddress">
                    </div>
                    <div ng-show="accountRegisterForm.$submitted || accountRegisterForm.mailaddress.$touched">
                        <p class="errorMessage" ng-show="accountRegisterForm.mailaddress.$error.required">メールアドレスを入力してください</p>
                    </div>
                    <div ng-show="accountRegisterForm.$submitted || accountRegisterForm.mailaddress.$touched">
                        <p class="errorMessage" ng-show="accountRegisterForm.mailaddress.$error.email">入力形式に誤りがあります。</p>
                    </div>                    
                    <div class="accountRegister-form__item form__item">
                        <input type="text" name="username" ng-required="true" placeholder="ユーザネーム" ng-model="accountRegister.username" maxlength="30">
                    </div>
                    <div ng-show="accountRegisterForm.$submitted || accountRegisterForm.username.$touched">
                        <p class="errorMessage" ng-show="accountRegisterForm.username.$error.required">ユーザネームを入力してください</p>
                    </div>
                    <div class="accountRegister-form__item form__item">
                        <input type="password" name="password" ng-required="true" placeholder="パスワード" ng-model="accountRegister.password" >
                    </div>
                    <div ng-show="accountRegisterForm.$submitted || accountRegisterForm.password.$touched">
                        <p class="errorMessage" ng-show="accountRegisterForm.password.$error.required">パスワードを入力してください</p>
                    </div>
                    <div class="accountRegister-form__item form__item">
                        <input type="checkbox" name="term" ng-required="true" ng-model="accountRegister.term"><a ui-sref="term">利用規約</a>に同意する
                    </div>
                    <div ng-show="accountRegisterForm.$submitted || accountRegisterForm.term.$touched">
                        <p class="errorMessage" ng-show="accountRegisterForm.term.$error.required">利用規約に同意できない場合はアカウント作成できません。</p>
                    </div> 
                    <div class="accountRegister-form__item form__item">
                        <div class="accountRegister-form__submit button_type_square">
                            <button>
                                <span>アカウント作成</span>
                            </button>
                        </div>
                    </div>
                    <div class="accountRegister-form__item form__item">
                        <div class="accountRegister-form__submit button_type_square">
                            <a ui-sref="login">
                                <span>戻る</span>
                            </a>
                        </div>
                    </div>                    
                </form>
                <div class="accountRegister__message">
                    {{accountRegister.message}}
                </div>                
            </div>
`