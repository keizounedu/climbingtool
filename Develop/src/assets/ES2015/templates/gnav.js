module.exports.tpl = `
            <ul class="gnav__items">
                <li class="gnav__item">
                    <a ui-sref="stream">
                        <img src="assets/img/common/icon/folder.png" width="25" height="25" alt="explore">
                    </a>
                </li>               
                <li class="gnav__item">
                    <a ui-sref="projectMaker">
                        <img src="assets/img/common/icon/edit.png" width="25" height="25" alt="create">
                    </a>                
                </li>
                <li class="gnav__item">
                    <a ui-sref="projectRegister">
                        <img src="assets/img/common/icon/upload-1.png" width="25" height="25" alt="add">
                    </a>
                </li>                

                <li class="gnav__item">
                    <a ui-sref="userInfo({userId:'{{gnav.username}}'})">                
                        <img src="assets/img/common/icon/avatar.png" width="25" height="25" alt="home">
                    </a>
                </li>
                <li class="gnav__item">
                    <a ui-sref="accountEdit"> 
                        <img src="assets/img/common/icon/settings.png" width="25" height="25" alt="">
                    </a>
                </li>
                <li class="gnav__item">
                    <a ng-click="gnav.$commonService.logout()"> 
                        <img src="assets/img/common/icon/logout.png" width="25" height="25" alt="">
                    </a>
                </li>
            </ul>
`