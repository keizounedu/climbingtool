module.exports.tpl = `
                <div class="projectRegister">
                    <form ng-submit="projectRegisterForm.$valid && projectRegister.register()" novalidate name="projectRegisterForm" class="projectRegister-form form" id="projectRegister-form" enctype="multipart/form-data">
                        <div class="projectRegister-form__item form__item">
                            <span>課題の写真：</span>
                            <input type="file" name="file[]" id="file" multiple="" ng-required="true">
                        </div>
                        <div class="projectRegister-form__item form__item">
                            <span>課題名：</span>
                            <input type="text" name="projectName" ng-required="true" ng-model="projectRegister.projectName">
                        </div>
                        <div ng-show="projectRegisterForm.$submitted || projectRegisterForm.projectName.$touched">
                            <p class="errorMessage" ng-show="projectRegisterForm.projectName.$error.required">課題名は必須です。</p>
                        </div>                        
                        <div class="projectRegister-form__item form__item">
                            <span>場所：</span>
                            <input type="text" name="projectPlace" ng-required="true" ng-model="projectRegister.projectPlace">
                        </div>
                        <div ng-show="projectRegisterForm.$submitted || projectRegisterForm.projectPlace.$touched">
                            <p class="errorMessage" ng-show="projectRegisterForm.projectPlace.$error.required">場所は必須です。</p>
                        </div>
                        <div class="projectRegister-form__item form__item">
                            <span>グレード：</span>
                            <select name="projectGrade" ng-required="true" ng-model="projectRegister.projectGrade" ng-options="grade for grade in projectRegister.projectGradeList">
                                <option value="">グレードを選択してください</option>
                            </select>
                        </div>
                        <div ng-show="projectRegisterForm.$submitted || projectRegisterForm.projectGrade.$touched">
                            <p class="errorMessage" ng-show="projectRegisterForm.projectGrade.$error.required">グレードは必須です。</p>
                        </div>                                      
                        <div class="projectRegister-form__item form__item item_type_radio">
                            <span>プライバシー設定：</span>
                            公開：<input type="radio" ng-model="projectRegister.isPublic" value="true" name="isPublic">&nbsp;
                            自分のみ：<input type="radio" ng-model="projectRegister.isPublic" value="false" name="isPublic">
                        </div>
                        <div class="errorMessage">
                        {{projectRegister.message}}
                        </div>
                        <div class="projectRegister-form__item projectRegister-form__submit button_type_circle">
                            <button>
                                <span>作成</span>
                            </button>
                        </div>
                    </form>
                </div>
`