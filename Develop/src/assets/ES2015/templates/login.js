module.exports.tpl = `
            <div class="login">
                <form novalidate class="login-form" name="loginForm">
                    <div class="login-form__item">
                        <input type="text" name="username" ng-model="login.username" ng-required="true" ng-maxlength="30" placeholder="ユーザネーム">
                    </div>
                    
                    <div ng-show="loginForm.$submitted || loginForm.username.$touched">
                        <p class="errorMessage" ng-show="loginForm.username.$error.required">ユーザネームを入力してください</p>
                    </div>

                    <div class="login-form__item">
                        <input type="password" name="password" ng-model="login.password" ng-required="true" placeholder="パスワード">
                    </div>

                    <div ng-show="loginForm.$submitted || loginForm.password.$touched">
                        <p class="errorMessage" ng-show="loginForm.password.$error.required">パスワードを入力してください</p>
                    </div>

                    <div class="login-form__item login-form__submit button_type_square">
                        <button ng-click="login.login()">ログイン</button>
                    </div>
                </form>
                <div class="login__createUser button_type_square">
                    <a ui-sref="accountRegister">
                        <span>アカウント作成</span>
                    </a>
                </div>
                <div class="login__message">
                    <p>{{login.message}}<p>
                    <div ng-show="loginForm.$submitted">
                        <p ng-show="loginForm.username.$error.required || loginForm.password.$error.required">必須項目が入力されていません。</p>
                    </div>
                </div>
            </div>
`