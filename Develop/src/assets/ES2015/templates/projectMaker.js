    module.exports.tpl = `
        <div class="create">
                <div class="create__item">
                    <div class="create__imageInput">
                        <p>
                            クライミングウォールの写真を選択してください。
                        </p>
                        <form enctype="multipart/form-data" name="projectcreate__form" id="projectcreate__form">
                            <div>
                                ファイルを選択
                                <input type="file" name="file[]" multiple="" id="fileInput">
                            </div>
                        </form>
                    </div>
                    <div class="create__image">
                        <div class="create__editorCtrl">
                            <ul class="create__iconList">

                                <li class="create__iconItem">
                                    <a class="element-add" data-colortype="r" data-marktype="circle__s"><img src="/assets/img/create/r/circle__s.png" alt="" ></a>
                                </li>
                                <li class="create__iconItem">
                                    <a class="element-add" data-colortype="g" data-marktype="circle__s"><img src="/assets/img/create/g/circle__s.png" alt="" ></a>
                                </li>
                                <li class="create__iconItem">
                                    <a class="element-add" data-colortype="b" data-marktype="circle__s"><img src="/assets/img/create/b/circle__s.png" alt="" ></a>
                                </li>

                                <li class="create__iconItem">
                                    <a class="element-add" data-colortype="r" data-marktype="circle__r"><img src="/assets/img/create/r/circle__r.png"  alt=""></a>
                                </li>
                                <li class="create__iconItem">
                                    <a class="element-add" data-colortype="g" data-marktype="circle__r"><img src="/assets/img/create/g/circle__r.png"  alt=""></a>
                                </li>
                                <li class="create__iconItem">
                                    <a class="element-add" data-colortype="b" data-marktype="circle__r"><img src="/assets/img/create/b/circle__r.png"  alt=""></a>
                                </li>
                                
                                <li class="create__iconItem">
                                    <a class="element-add" data-colortype="r" data-marktype="circle__g"><img src="/assets/img/create/r/circle__g.png" alt=""></a>
                                </li>
                                <li class="create__iconItem">
                                    <a class="element-add" data-colortype="g" data-marktype="circle__g"><img src="/assets/img/create/g/circle__g.png" alt=""></a>
                                </li>
                                <li class="create__iconItem">
                                    <a class="element-add" data-colortype="b" data-marktype="circle__g"><img src="/assets/img/create/b/circle__g.png" alt=""></a>
                                </li>
                                
                                <li class="create__iconItem type_of_kante">
                                    <a class="element-add" data-colortype="r" data-marktype="kante"><img src="/assets/img/create/r/kante.png"  alt=""></a>
                                </li>                                
                                <li class="create__iconItem type_of_kante">
                                    <a class="element-add" data-colortype="g" data-marktype="kante"><img src="/assets/img/create/g/kante.png"  alt=""></a>
                                </li>                                
                                <li class="create__iconItem type_of_kante">
                                    <a class="element-add" data-colortype="b" data-marktype="kante"><img src="/assets/img/create/b/kante.png"  alt=""></a>
                                </li>

                                <li class="create__iconItem">
                                    <a class="element-add" data-colortype="r" data-marktype="bote"><img src="/assets/img/create/r/bote.png"  alt=""></a>
                                </li>
                                <li class="create__iconItem">
                                    <a class="element-add" data-colortype="g" data-marktype="bote"><img src="/assets/img/create/g/bote.png"  alt=""></a>
                                </li>
                                <li class="create__iconItem">
                                    <a class="element-add" data-colortype="b" data-marktype="bote"><img src="/assets/img/create/b/bote.png"  alt=""></a>
                                </li>                                                                
                            </ul>
                        </div>
                        <div class="create__imageEditor">
                            <p class="create__imageBase"><img src="/assets/img/create/base.jpg" alt="" id="base_img"></p>
                        </div>
                    </div>
                    <div class="create__btns cf">
                        <div class="create__registerBtn">
                            <p class="create__registerLink">作成する</p>
                        </div>
                    </div>
                </div>
        </div>
        <div class="overlay">
            <div class="overlay__bg overlay__close"></div>
            <p class="overlay__closeBtn overlay__close">
                <img src="assets/img/common/icon/cancel.png" width="50" height="50" alt="">
            </p>
            <div class="overlay__result">
                <img src="" class="overlay__resultImg"></img>
                <canvas id="phantom_cnv"></canvas>
            </div>
        </div>
    `;
