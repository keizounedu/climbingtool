    module.exports.ctrl = class accountRegister_ctrl {
        /* @ngInject */
        constructor(commonService, accountRegisterService) {
            this.cashElements(commonService, accountRegisterService)
        }
        cashElements(commonService, accountRegisterService) {
            this.$commonService = commonService
            this.$accountRegisterService = accountRegisterService
            this.username = ""
            this.password = ""
            this.mailaddress = ""
            this.term = false
            this.message = ""
            this.fd = ""
        }
        register() {
            (() => {
                this.$commonService.loader_visible()
                this.fd = new FormData(document.getElementById("accountRegister-form"));
                this.$commonService.$q.all([this.$accountRegisterService.register(this.fd)])
                    .then((res) => {
                        if (res == "complete") {
                            this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.accoutResiger_msg_200())
                        } else {
                            this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.accoutResiger_msg_400())
                        }
                        this.$commonService.loader_inVisible()                        
                        this.$commonService.$state.go('login')
                    })
            })()

        }
    }
