    module.exports.ctrl = class projectmaker_ctrl {
        /* @ngInject */
        constructor(imageGenarator, createElements, commonService) {
            imageGenarator.cashElements()
            imageGenarator.bindEvents()
            createElements.cacheElements();
            createElements.bindEvents();
        }
    }
