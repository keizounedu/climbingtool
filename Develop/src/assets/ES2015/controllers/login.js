    module.exports.ctrl = class login_ctrl {
        /* @ngInject */
        constructor(commonService, loginService) {
            this.cashElements(commonService, loginService)
        }
        cashElements(commonService, loginService) {
            this.$commonService = commonService
            this.$commonService.$cookieService.haveCookieCheck()
            this.$loginService = loginService
            this.username = ""
            this.password = ""
            this.message = ""
        }
        login() {
            this.$commonService.loader_visible()
            this.$commonService.$q.all([this.$loginService.login(this.username, this.password)])
                .then((msg) => {
                    this.message = msg[0]
                    this.$commonService.loader_inVisible()
                })
        }
    }
