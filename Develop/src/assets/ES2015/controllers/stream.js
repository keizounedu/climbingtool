    module.exports.ctrl = class stream_ctrl {
        /* @ngInject */
        constructor(commonService, streamService) {
            this.cashElements(commonService, streamService)
            this.getData()
        }
        cashElements(commonService, streamService) {
            this.$commonService = commonService
            this.$streamService = streamService
            this.username = this.$commonService.$cookieService.$cookies.get("loginId")
        }
        getData() {
            this.$commonService.loader_visible()
            this.$commonService.$q.all([this.$streamService.getProjectInfo()])
                .then((data) => {
                    this.date = new Date().getTime()
                    this.projectList = data[0]
                    this.$commonService.loader_inVisible()
                })
        }
    }
