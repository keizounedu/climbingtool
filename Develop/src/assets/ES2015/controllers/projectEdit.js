    module.exports.ctrl = class projectEdit_ctrl {
        /* @ngInject */
        constructor(commonService, projectEditService) {

            if (commonService.$state.params.projectId == undefined) {
                commonService.$state.go("userInfo", { "userId": commonService.$cookies.get("loginId") })
            }
            commonService.$cookieService.$cookies.put("editProject", String(commonService.$state.params.projectId), ["expires", 7])

            this.cashElements(commonService, projectEditService)
            this.getData()

        }
        cashElements(commonService, projectEditService) {

            this.$commonService = commonService
            this.$projectEditService = projectEditService

            this.loginId = this.$commonService.$cookies.get("loginId")

            this.projectId = this.$commonService.$state.params.projectId
            this.projectImg = ""
            this.projectName = ""
            this.projectPlace = ""
            this.projectGrade = ""
            this.projectGradeList = this.$commonService.projectGrade
            this.projectGrade = this.projectGradeList[2]
            this.isPublic = "false"
            this.message = ""
            this.fd = ""

        }
        getData() {
            this.$commonService.loader_visible()
            this.$commonService.$q.all([this.$projectEditService.getProjectInfo({ _id: this.projectId })])
                .then((data) => {
                    if (data[0][0].projectAuthor != this.loginId) {
                        this.$commonService.$state.go("userInfo", { "userId": this.loginId })
                    }
                    this.projectImg = data[0][0].projectImg
                    this.projectName = data[0][0].projectName
                    this.projectPlace = data[0][0].projectPlace
                    this.isPublic = String(data[0][0].isPublic)
                    this.projectGradeList.forEach((element, index, array) => {
                        if (element == data[0][0].projectGrade) {
                            this.projectGrade = this.projectGradeList[index]
                        }
                    })
                    this.$commonService.loader_inVisible()
                })
        }
        edit() {
            this.$commonService.loader_visible()
            this.fd = new FormData(document.getElementById("projectEdit-form"));
            this.$commonService.$q.all([this.$projectEditService.edit(this.fd)])
                .then((res) => {
                    if (res) {
                        this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.projectEdit_msg_200())
                    } else {
                        this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.projectEdit_msg_400())
                    }
                    this.$commonService.loader_inVisible()
                    this.$commonService.$state.go("userInfo", { "userId": this.$commonService.$cookieService.$cookies.get("loginId") })

                })
        }
        delete() {
            this.$commonService.loader_visible()
            if (window.confirm('プロジェクトを削除しますがよろしいですか？')) {
                this.$commonService.$q.all([this.$projectEditService.delete({ _id: this.projectId })])
                    .then((res) => {
                        if (res) {
                            this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.projectDelete_msg_200())
                        } else {
                            this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.projectDelete_msg_400())
                        }
                        this.$commonService.loader_inVisible()
                        this.$commonService.$state.go("userInfo", { "userId": this.$commonService.$cookieService.$cookies.get("loginId") })
                    })
            } else {
                this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.function_interruption_msg())
                this.$commonService.loader_inVisible()
            }
        }
    }
