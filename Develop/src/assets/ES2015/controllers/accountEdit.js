module.exports.ctrl = class accountEdit_ctrl {
    /* @ngInject */
    constructor(commonService, accountEditService) {
        this.cashElements(commonService, accountEditService)
        this.bindEvents()
        this.getData()
    }
    cashElements(commonService, accountEditService) {

        this.$commonService = commonService
        this.$accountEditService = accountEditService

        this.file = document.getElementById("file")
        this.file_entity = undefined

        this._id = ""
        this.username = this.$commonService.$cookieService.$cookies.get("loginId")
        this.password = ""
        this.mailAddress = ""
        this.iconImg = ""
        this.hpUrl = ""
        this.introduction = ""
        this.message = ""
        this.fd = ""
    }
    bindEvents() {
        this.file.addEventListener('change', (e) => {
            var target = e.target,
                files = target.files;
            this.file_entity = files[0]
        });
    }
    getData() {
        this.$commonService.loader_visible()
        this.$commonService.$q.all([this.$accountEditService.getUserInfo(this.username)])
            .then((data) => {
                this._id = data[0]._id
                this.password = data[0].password
                this.mailAddress = data[0].mailAddress
                this.iconImg = data[0].iconImg
                this.hpUrl = data[0].hpUrl
                this.introduction = data[0].introduction
                this.$commonService.loader_inVisible()
            })
    }
    edit() {

        this.$commonService.loader_visible()


        this.fd = new FormData(document.getElementById("accountEdit-form"));
        this.fd.append('username', String(this.username))

        if (this.file_entity == undefined) {
            this.message = ''
            this.$commonService.$q.all([this.$accountEditService.edit(this.fd)])
                .then((res) => {
                    if (res) {
                        this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.accoutEdit_msg_200())
                    } else {
                        this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.accoutEdit_msg_400())
                    }
                    this.$commonService.loader_inVisible()

                })
        } else {
            if (!this.file_entity.name.match(/\.(JPG|JPEG|PNG|jpg|jpeg|png)$/) == "") {
                this.message = ''
                this.$commonService.$q.all([this.$accountEditService.edit(this.fd)])
                    .then((res) => {
                        if (res) {
                            this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.accoutEdit_msg_200())
                        } else {
                            this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.accoutEdit_msg_400())
                        }
                        this.$commonService.loader_inVisible()
                        this.$commonService.$state.go("userInfo", { "userId": this.$commonService.$cookieService.$cookies.get("loginId") })
                    })
            } else {
                this.message = 'プロフィール写真にはjpg,pngのみ使用可能です。'
                this.$commonService.loader_inVisible()
            }
        }
    }
    delete() {
        this.$commonService.loader_visible()
        if (window.confirm('アカウントを削除しますがよろしいですか？')) {
            this.$commonService.$q.all([this.$accountEditService.delete({ _id: this._id })])
                .then((res) => {
                    if (res) {
                        this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.accountDelete_msg_200())
                    } else {
                        this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.accountDelete_msg_400())
                    }
                    this.$commonService.loader_inVisible()
                    this.$commonService.logout()
                })
        } else {
            this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.function_interruption_msg())
            this.$commonService.loader_inVisible()
        }
    }
}
