    module.exports.ctrl = class user_ctrl {
        /* @ngInject */
        constructor(commonService, userService) {
            this.cashElements(commonService, userService)
            this.getData()
        }
        cashElements(commonService, userService) {
            this.$commonService = commonService
            this.$userService = userService

            this.loginId = this.$commonService.$cookieService.$cookies.get("loginId")
            this.username = this.$commonService.$state.params.userId
            this.iconImg = ""
            this.hpUrl = ""
            this.introduction = ""
            this.projectList = ""

            this.date = new Date().getTime()

        }
        getData() {
            this.$commonService.loader_visible()            
            this.$commonService.$q.all([this.$userService.getUserInfo(this.username), this.$userService.getProjectInfo({ projectAuthor: this.username })])
                .then((data) => {
                    this.username = data[0].username
                    this.iconImg = data[0].iconImg;
                    this.hpUrl = data[0].hpUrl
                    this.introduction = data[0].introduction
                    if (!data[1].length == 0) {
                        this.date = new Date().getTime()
                        this.projectList = data[1]
                    }
                    this.$commonService.loader_inVisible()
                })
        }

    }
