    module.exports.ctrl = class projectRegister_ctrl {
        /* @ngInject */
        constructor(commonService, projectRegisterService) {
            this.cashElements(commonService, projectRegisterService)
            this.bindEvents()
        }
        cashElements(commonService, projectRegisterService) {

            this.$commonService = commonService
            this.$projectRegisterService = projectRegisterService

            this.file = document.getElementById("file")
            this.file_entity = undefined

            this.projectName = ""
            this.projectPlace = ""
            this.projectGrade = ""
            this.projectGradeList = this.$commonService.projectGrade
            this.isPublic = "false"
            this.message = ""
            this.fd = ""
        }
        bindEvents() {
            this.file.addEventListener('change', (e) => {
                var target = e.target,
                    files = target.files;
                this.file_entity = files[0]
            });
        }
        register() {
            this.$commonService.loader_visible()
            this.fd = new FormData(document.getElementById("projectRegister-form"));
            this.fd.append('projectAuthor', this.$commonService.$cookieService.$cookies.get("loginId"))
            if (this.file_entity == undefined) {
                this.$commonService.$q.all([this.$projectRegisterService.register(this.fd)])
                    .then((res) => {
                        if (res) {
                            this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.projectRegister_msg_200())
                        } else {
                            this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.projectRegister_msg_400())
                        }
                        this.$commonService.loader_inVisible()
                        this.$commonService.$state.go("userInfo", { "userId": this.$commonService.$cookieService.$cookies.get("loginId") })
                    })
            } else {
                if (!this.file_entity.name.match(/\.(JPG|JPEG|PNG|jpg|jpeg|png)$/) == "") {
                    this.$commonService.$q.all([this.$projectRegisterService.register(this.fd)])
                        .then((res) => {
                            if (res) {
                                this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.projectRegister_msg_200())
                            } else {
                                this.$commonService.$pushNotificationService.output(this.$commonService.$messageService.projectRegister_msg_400())
                            }
                            this.$commonService.loader_inVisible()

                            this.$commonService.$state.go("userInfo", { "userId": this.$commonService.$cookieService.$cookies.get("loginId") })
                        })
                } else {
                    this.message = 'プロジェクトの写真にはjpg,pngのみ使用可能です。'
                    this.$commonService.loader_inVisible()
                }

            }
        }
    }
