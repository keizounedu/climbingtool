(function() {

    // init
    angular.module('App', ['ui.router','ngCookies','ngclipboard','ngAnimate','ngTouch'])

    // Service
    require("./service/http.js")
    require("./service/cookieService.js")
    require("./service/imageGenarator.js")
    require("./service/createElement.js")

    require("./service/commonService.js")
    require("./service/pushNotificationService.js")            
    require("./service/messageService.js")
    require("./service/loginService.js")    
    require("./service/userService.js")        
    require("./service/streamService.js")        
    require("./service/accountEditService.js")            
    require("./service/accountRegisterService.js")                
    require("./service/projectRegisterService.js")            
    require("./service/projectEditService.js")            

    // Components
    require("./components/login.js")
    require("./components/term.js")    
    require("./components/accountRegister.js")
    require("./components/accountEdit.js")
    require("./components/user.js")
    require("./components/stream.js")
    require("./components/projectRegister.js")
    require("./components/projectEdit.js")    
    require("./components/projectmaker.js")
    require("./components/pageTitle.js")
    require("./components/gnav.js")
    require("./components/pushNotification.js")

    // Router
    require("./router/router.js")

})();   
